import React, {Component} from 'react';
import { StyleSheet, Text, View,ImageBackground,Platform,KeyboardAvoidingView} from 'react-native';
import Header from './../../Components/Header/index';
import moment from 'moment';
import 'moment/locale/it'
import TrainingDetails from './../../Components/TrainingDetails/index'
import SafeAreaView from 'react-native-safe-area-view';
import { ScrollView } from 'react-native-gesture-handler';
import Comments from './../../Components/Comments/index'

export default class TrainingDetailsScreen extends Component {
constructor(props){
  super(props)
  this.state={
    refresh:false
  }
}

changeRefresh(refresh){
  this.setState({refresh})
}
    render() {
      const { navigation } = this.props;
      const training_id = navigation.getParam('training_id', '');
      const date = navigation.getParam('date', '');

        return (

            <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                  <Header title="Allenamenti" back one navigation={this.props.navigation} />
                  <View style={styles.info}>
                  <Text style={styles.date}>{date}</Text>
                  </View>
            </View>
            <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null} style={styles.body} keyboardVerticalOffset={0}>
            <ImageBackground source={require('./../../assets/images/fondo_sfumato.png')} style={styles.body}>
            <ScrollView>
              <TrainingDetails training_id={training_id} />
              <View>
                <Comments training_id={training_id} refresh={this.state.refresh} changeRefresh={this.changeRefresh.bind(this)} />
              </View>
              </ScrollView>
            </ImageBackground>
             </KeyboardAvoidingView>
            </SafeAreaView>
          )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    header: {
      display:'flex',
      height:100
    },
    body: {
      flex:10
    }, 
    info:{
      flex:1,
      justifyContent:'flex-end', 
      alignSelf:'center', 
      alignItems:'center', 
      marginBottom:'4%'
    },
    date:{
      fontSize:22, 
      fontFamily:'Montserrat-Medium', 
    }
  });
