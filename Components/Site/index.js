//profile list
import React, {Component} from 'react';
import { View,Text, TouchableOpacity,Linking } from 'react-native';
import styles from './styles'
import { Divider } from 'react-native-elements';
import Colors from './../../Constants/Colors'


export default class Site extends Component {

    render() {
      return (
        <View>
            <View style={styles.box}>
                <TouchableOpacity
                onPress={() => {
                    Linking.canOpenURL('https://inforyou.teamsystem.com/areadonna/').then(supported => {
                        if (supported) {
                          Linking.openURL('https://inforyou.teamsystem.com/areadonna/');
                        } else {
                          alert('Non è stato possibile aprire la pagina web!');
                        }
                      })
                      .catch(error => console.log(error));      
                }}
                >
                    <Text style={styles.text}>AREA RISERVATA</Text>
                </TouchableOpacity>
            </View>
            <Divider style={{ backgroundColor: Colors.white }} />
            <View style={styles.box}>
            <TouchableOpacity
                onPress={() => {
                    Linking.canOpenURL('https://s3-eu-west-3.amazonaws.com/mediasportpiu/wp-content/uploads/2018/09/25180433/APP-AREA-DONNA_Ottobre-2019-1.pdf').then(supported => {
                        if (supported) {
                          Linking.openURL('https://s3-eu-west-3.amazonaws.com/mediasportpiu/wp-content/uploads/2018/09/25180433/APP-AREA-DONNA_Ottobre-2019-1.pdf');
                        } else {
                          alert('Non è stato possibile aprire la pagina web!');
                        }
                      })
                      .catch(error => console.log(error))      
                }}
                >
                <Text style={styles.text}>I NOSTRI CORSI</Text>
            </TouchableOpacity>    
            </View>
            <Divider style={{ backgroundColor: Colors.white }} />
            <View style={styles.box}>
            <TouchableOpacity
                onPress={() => {
                    Linking.canOpenURL('http://www.centrisportpiu.it').then(supported => {
                        if (supported) {
                          Linking.openURL('http://www.centrisportpiu.it');
                        } else {
                          alert('Non è stato possibile aprire la pagina web!');
                        }
                      })
                      .catch(error => console.log(error))  
                }}
                >
                <Text style={styles.text}>VAI AL SITO</Text>
            </TouchableOpacity>    
            </View>
        </View>
      )
    }
}
  

 