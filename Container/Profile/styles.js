import { StyleSheet} from 'react-native';

export default StyleSheet.create({
    container:{
        flex: 1,
    },
    header:{
        display:'flex',
        alignItems:'flex-end', 
        flexDirection:'row',
    },
    body:{
        flex:6,
    }
  });