import React, {Component} from 'react';
import {View,StyleSheet,Text} from 'react-native';
import ChangeGender from './../../../Components/ChangeGender/index'
import SafeAreaView from 'react-native-safe-area-view';
export default class ChangeGenderScreen extends Component {
  constructor(props){
    super(props);    
  }

  static navigationOptions = {
    title:'Sesso',
  };

  render() {
    const { navigation } = this.props;
    const data = navigation.getParam('data', '');

      return (
        <SafeAreaView style={styles.container}>
            <ChangeGender gender={data} navigation={this.props.navigation} />
        </SafeAreaView>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
  });
