import {  AUTH_SET_TOKEN, AUTH_REMOVE_TOKEN,GET_USER_DATA, SET_PROFILE_PHOTO,SET_USER_NAME,SET_USER_SURNAME,SET_USER_PHONE,SET_USER_EMAIL,SET_USER_GENDER,SET_USER_DATEOFBIRTH, SET_USER_POINTS,SET_USER_NICKNAME,SET_USER_PALESTRE,REFRESH_USER_SESSIONS } from "./actionTypes";
import { uiStartLoading, uiStopLoading } from "./../actions/ui";
import { showMessage, hideMessage } from "react-native-flash-message";
import axios from 'axios'
import { url } from './../../Constants/url'

export const tryAuth = (authData,palestre) => {
  return dispatch => {
    console.log(url[palestre])
    dispatch(uiStartLoading());
    axios.post(url[palestre].base + '/api/auth/login', {
        email: authData.email,
        password: authData.password,
    })
    .then(response => {
      dispatch(uiStopLoading());
      console.log(response)
      if (!response.data.token) {
        alert("Authentication failed, please try again!");
      } else {
        dispatch(setUserPalestre(palestre))
        dispatch(authSetToken(response.data.token));
        dispatch(setProfilePhoto(response.data.user.img_src))
        dispatch(setUserName(response.data.user.nome))
        dispatch(setUserNickname(response.data.user.nickname))
        dispatch(setUserSurname(response.data.user.cognome))
        dispatch(setUserPhone(response.data.user.cellulare))
        dispatch(setUserEmail(response.data.user.email))
        dispatch(setUserGender(response.data.user.sesso))
        dispatch(setUserDateOfBirth(response.data.user.nato_il))
        dispatch(setUserPoints(response.data.user.points))
      }
    })
    .catch(err => {
      dispatch(uiStopLoading());
      console.log(err)
        if(!err.response){
          showMessage({
            message: 'Non è stato possibile fare login',
            type: "danger",
          })
          return ;
        }
        else if(err.response.data && err.response.data.errors && err.response.data.errors.email){
          showMessage({
            message: err.response.data.errors.email,
            type: "danger",
          });
        }
        else{
          showMessage({
            message: err.response.data && err.response.data.errors,
            type: "danger",
          });
        }
      });
  };
};

export const authSetToken = token => {
  return {
    type: AUTH_SET_TOKEN,
    token: token
  };
};

export const authRemoveToken = () => {
  return {
    type: AUTH_REMOVE_TOKEN,
    token: null
  }
}

export const setProfilePhoto = src => {
  return {
    type: SET_PROFILE_PHOTO,
    img_src: src
  }
}


export const setUserName = name => {
  return {
    type: SET_USER_NAME,
    name: name
  }
}

export const setUserSurname = surname => {
  return {
    type: SET_USER_SURNAME,
    surname: surname
  }
}

export const setUserPhone = phone => {
  return {
    type:SET_USER_PHONE,
    phone: phone
  }
}

export const setUserEmail = email => {
  return {
    type:SET_USER_EMAIL,
    email: email
  }
}

export const setUserGender = gender => {
  return {
    type:SET_USER_GENDER,
    gender: gender
  }
}

export const setUserDateOfBirth = dateofbirth => {
  return {
    type:SET_USER_DATEOFBIRTH,
    dateofbirth: dateofbirth
  }
}

export const setUserPoints = points => {
  return {
    type:SET_USER_POINTS,
    points: points
  }
}


export const setUserNickname = nickname => {
  return {
    type:SET_USER_NICKNAME,
    nickname:nickname
  }
}

export const setUserPalestre = palestre => {
  return {
    type:SET_USER_PALESTRE,
    palestre:palestre
  }
}

export const getUserData = user => {
  return{
    type: GET_USER_DATA,
    user: user
  }
}


export const refreshUS = refresh => {
  return {
    type: REFRESH_USER_SESSIONS,
    refresh: refresh
  }
}
