import React, {Component} from 'react';
import { View,TouchableOpacity,Text} from 'react-native';
import styles from './styles'
import moment from 'moment'
import DateTimePicker from 'react-native-modal-datetime-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import instance from './../../api/instance'
import { connect } from "react-redux";
import { setUserDateOfBirth } from "./../../store/actions/auth";
import { bindActionCreators } from "redux";
import Header from './../Header/index'
import { showMessage, hideMessage } from "react-native-flash-message";


class ChangeDateOfBirth extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            isLoading:null,
            date: this.props.dateofbirth == '...' ? moment() : moment(this.props.dateofbirth,'DD/MM/YYYY'),
            isDateTimePickerVisible: false
        }
    }

    send(){
        this.setState({isLoading:true})
        instance.post('user/update', {
            nato_il: moment(this.state.date).format('YYYY-MM-DD')
        })
        .then(response => {
            this.props.setUserDateOfBirth(moment(this.state.date).format('YYYY-MM-DD'))
            this.setState({isLoading:false})
            this.props.navigation.goBack()
        })
        .catch(err => {
            showMessage({
                message:err.response ? err.response.data.msg : 'La modifica non è andata a buon fine',
                type: "danger",
              });
        })
        // setTimeout(() =>{
        //     this.setState({isLoading:false})
        // },3000);
    }


    showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    handleDatePicked = (date) => {
        this.setState({date : moment(date)});
        this.hideDateTimePicker();
    };


    render() {
      return (
        <View style={styles.container}>
            <Spinner
            visible={this.state.isLoading}
            textContent={'Salvando...'}
            color={"#f3f3f3"}
            textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.header}>
            <Header title="Data di nascita" back one navigation={this.props.navigation} />
            </View>
            <View style={styles.body}>
            <View style={styles.top}>
                <View style={styles.icontainer}>
                    <Text style={styles.info}>Modifica la tua data di nascita</Text>
                    <TouchableOpacity
                    activeOpacity={0.8} 
                    onPress={this.showDateTimePicker}
                    >
                    <View style={styles.input}>
                        <Text style={styles.date}>{moment(this.state.date).format('DD/MM/YYYY')}</Text>
                    </View>
                    </TouchableOpacity>
                    <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    titleIOS={"Scegli la data"}
                    mode={"date"}
                    confirmTextIOS={"Scegli"}
                    cancelTextIOS={"Anulla"}
                    datePickerModeAndroid={'spinner'}
                    onConfirm={this.handleDatePicked}
                    onCancel={this.hideDateTimePicker}
                    date ={this.state.date.isValid()?this.state.date.toDate():moment().toDate()}
                    placeholder=" "
                />
                </View>
            </View>
            <View style={styles.bottom}>
                <View style={[styles.bcontainer]}>
                    <TouchableOpacity
                    activeOpacity={0.8} 
                    style={styles.button}
                    onPress={() => {
                        if(this.state.date != ''){
                        this.send()
                        }
                    }}>
                        <Text style={styles.text}>Salva</Text>
                    </TouchableOpacity>
                </View>          
            </View>
           </View>
        </View>
      )
    }
  }


const mapDispachToProps = dispatch => {
    return bindActionCreators({
        setUserDateOfBirth
   },dispatch)
}

export default connect(null,mapDispachToProps)(ChangeDateOfBirth)

 