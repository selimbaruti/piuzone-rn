import { StyleSheet, Platform, Dimensions } from 'react-native'

export default StyleSheet.create({
    container: {
        flex:1,
    },
    loader:{
        marginTop:'4%'
    },
    message:{
        fontFamily:'Montserrat-Medium',
        textAlign:'center',
        fontSize:22,
        marginTop:10,
        color:'#cccccc'
    }
})
