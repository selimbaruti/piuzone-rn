import React, {Component} from 'react';
import { View, Text} from 'react-native';
import styles from './styles'
import Header from './../../Components/Header/index'
import BiometricData from './../../Components/BiometricData/index'

export default class Biometric extends Component {

    render() {
      return (
        <View style={styles.container}>
            <View style={styles.header}>
            <Header title="Biometria" back one navigation={this.props.navigation} />
            </View>
            <View style={styles.body}>
                <BiometricData />
            </View>
        </View>
      )
    }
  }
  

