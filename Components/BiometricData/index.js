//profile list
import React, {Component} from 'react';
import { View,Text,TouchableOpacity,ScrollView } from 'react-native';
import styles from './styles'
import Colors from './../../Constants/Colors'
import RangeSlider from 'rn-range-slider';
import instance from './../../api/instance';
import { showMessage, hideMessage } from "react-native-flash-message";
import Spinner from 'react-native-loading-spinner-overlay';

let weight = 0;
let height = 0;
let hr_max = 0;
let tgzMin = 65;
let tgzMax = 85;

export default class BiometricData extends Component {

  constructor(props){
    super(props);

    this.state = {
        isLoading:false,
        height:null,
        hr_max:null,
        tgzMin:null,
        tgzMax:null,
        weight:null,
    }
  }


  async componentDidMount(){
    this.setState({isLoading:true});
    instance.get('biometrics/getData').then(response =>{
      weight = response.data.biometrics.weight
      height = response.data.biometrics.height
      hr_max = response.data.biometrics.hr_max
      tgzMin = response.data.biometrics.tg_zone_min
      tgzMax = response.data.biometrics.tg_zone_max;

      this.refs._weight.setLowValue(weight);
      this.refs._height.setLowValue(height);
      this.refs._hrMax.setLowValue(hr_max);
      this.refs._tgMinMax.setLowValue(tgzMin);
      this.refs._tgMinMax.setHighValue(tgzMax);

      this.setState({
        weight:response.data.biometrics.weight,
        height:response.data.biometrics.height,
        hr_max:response.data.biometrics.hr_max,
        tgzMin:response.data.biometrics.tg_zone_min,
        tgzMax:response.data.biometrics.tg_zone_max,
      });           
    }).catch().then(() => this.setState({isLoading:false}))
  }

  save = () => {
      
    this.setState({isLoading:true});
    instance.post('biometrics/update',{
      height:this.state.height,
      weight:this.state.weight,
      maxHr:this.state.hr_max,
      tgZoneMin:this.state.tgzMin,
      tgZoneMax:this.state.tgzMax
    }).then(resp => {
      showMessage({
        message:'Dati salvati!',
        type: "success",
        position:"top"
      });
      this.setState({isLoading:false});
    }).catch(e => {
      showMessage({
        message:'Non è stato possibile salvare',
        type: "danger",
        position:"top"
      });
      this.setState({isLoading:false});
    });
  }


    render() {              
      return (
        <View style={styles.container}>
          <Spinner
          visible={this.state.isLoading}
          textContent={'Caricando...'}
          color={"#f3f3f3"}
          textStyle={styles.spinnerTextStyle}
          />
          <ScrollView style={styles.top}>
            {/* pesso */}
            <View style={styles.box}>
            <View style={styles.info}>
              <Text style={styles.unit}>Peso {'\n'}({this.state.weight} Kg.)</Text>
            </View>
            <View style={styles.slider}>
              <RangeSlider
              ref="_weight"
              disableRange
              min={0}
              max={130}
              tintColor={Colors.white}
              handleBorderWidth={1}
              handleBorderColor={Colors.primary}
              selectionColor={Colors.primary}
              thumbBorderColor={Colors.primary}
              lowValue={weight}
              highValue={weight+1}
              style={{ flex: 1, height: 90, width:"90%", marginHorizontal:0 }}
              thumbColor={Colors.primary}
              handleColor={Colors.primary}
              textFormat={"%d Kg."}
              rangeEnabled={false}
              blankColor = {Colors.white}
              labelBackgroundColor={Colors.primary}
              labelBorderColor={Colors.primary}
              onValueChanged={ (low,high,userChange)=>{
              if(userChange)
              this.setState({
                weight:low,
              })
              }}
              />
            </View>
          </View>
            {/* altezza */}
            <View style={styles.box}>
            <View style={styles.info}>
              <Text style={styles.unit}>Altezza {'\n'}({this.state.height } Cm.)</Text>
            </View>
            <View style={styles.slider}>
              <RangeSlider
               ref="_height"
                disableRange
                min={0}
                max={230}
                tintColor={Colors.white}
                handleBorderWidth={1}
                handleBorderColor={Colors.primary}
                selectionColor={Colors.primary}
                thumbBorderColor={Colors.primary}
                lowValue={this.state.height}
                highValue={height +1 }
                style={{ flex: 1, height: 90, width:"90%", marginHorizontal:0 }}
                thumbColor	={Colors.primary}
                handleColor={Colors.primary}
                textFormat={"%d Cm"}
                rangeEnabled={false}
                blankColor = {Colors.white}
                labelBackgroundColor={Colors.primary}
                labelBorderColor={Colors.primary}
                onValueChanged={ (low,high,userChange)=>{
                  if(userChange)
                    this.setState({
                      height:low,
                    })
                  }}
                />
            </View> 
          </View>
            {/* bmp */}
            <View style={styles.box}>
            <View style={styles.info}>
              <Text style={styles.unit}>Bmp Max. {'\n'}({this.state.hr_max } Bmp.)</Text>
            </View>
            <View style={styles.slider}>
                <RangeSlider
                  ref="_hrMax"
                    disableRange
                    min={0}
                    max={230}
                    tintColor={Colors.white}
                    handleBorderWidth={1}
                    handleBorderColor={Colors.primary}
                    selectionColor={Colors.primary}
                    thumbBorderColor={Colors.primary}
                    lowValue={hr_max}
                    highValue={hr_max + 1 }
                    style={{ flex: 1, height: 90, width:"90%", marginHorizontal:0 }}                   
                    thumbColor	={Colors.primary}
                    handleColor={Colors.primary}
                    blankColor = {Colors.white}
                    labelBackgroundColor={Colors.primary}
                    labelBorderColor={Colors.primary}
                    textFormat={"%d Bpm"}
                    rangeEnabled={false}
                    onValueChanged={ (low,high,userChange)=>{
                      if(userChange)
                        this.setState({
                          hr_max:low,
                        
                        })
                    }}
                  />
            </View> 
          </View>
            {/* target zone */}
            <View style={styles.box}>
            <View style={styles.info}>
              <Text style={styles.unit}>Target Zone (%) {'\n'}({parseInt(this.state.tgzMin * this.state.hr_max / 100)} - {parseInt(this.state.tgzMax * this.state.hr_max / 100)} bpm)</Text>
            </View>
            <View style={styles.slider}>
                  <RangeSlider
                    ref="_tgMinMax"
                    min={0}
                    max={100}
                    
                    style={{ flex: 1, height: 90, width:"90%", marginHorizontal:0 }}                   
                    onValueChanged={ (low,high,userChange)=>{
                      if(userChange)
                        this.setState({
                          tgzMin:low,
                          tgzMax:high,
                        })
                    }}
                    thumbColor	={Colors.primary}
                    handleColor={Colors.primary}
                    blankColor = {Colors.white}
                    labelBackgroundColor={Colors.primary}
                    labelBorderColor={Colors.primary}
                    textFormat={ '%d%% Bpm Max'}
                    tintColor={Colors.white}
                    handleBorderWidth={1}
                    handleBorderColor={Colors.primary}
                    selectionColor={Colors.primary}
                    thumbBorderColor={Colors.primary}
                  />
            </View> 
          </View>
          </ScrollView> 
          <View style={styles.bottom}>
            <View style={[styles.bcontainer]}>
              <TouchableOpacity 
              activeOpacity={0.8}
              style={styles.button}
              onPress={() => this.save()}
              >
                        <Text style={styles.text}>Salva</Text>
               </TouchableOpacity>
            </View>
          </View>         
        </View>
      )
    }
  }
  

 