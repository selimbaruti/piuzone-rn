import { StyleSheet, Platform, Dimensions } from 'react-native'
import Fonts from './../../Common/Fonts'

const { width } = Dimensions.get('window')

export default StyleSheet.create({
    container: {
        flex:1,
        justifyContent:'center',
        alignItems: 'center',
        // width: width * 0.3, 
        height: width / 3 + 10,
    },
    //icon 
    icon: {
        width:50,
        height:50,
        resizeMode:'contain'
    },
    iconSmall: {
        width:50,
        height:50,
        marginBottom:5,
        resizeMode:'contain'
    },
    //info
    info: {
        fontSize: 22,
        fontFamily: Fonts.MontserratSemiBold
    },
    infoBig: {
        fontSize:45,
        lineHeight: 45,
        fontFamily: Fonts.MontserratSemiBold
    },
    //description
    description: {
        fontSize: 13,
        lineHeight:13,
        fontFamily: Fonts.MontserratSemiBold
    },
    descriptionBig: {
        fontSize: 20,
        lineHeight:20,
        fontFamily: Fonts.MontserratSemiBold
    },
    //time
    time: {
        fontSize:12,
        color:'grey',
        fontFamily: Fonts.MontserratSemiBold
    },
    timeBig: {
        fontSize:16,
        color: 'grey',
        fontFamily: Fonts.MontserratSemiBold
    },
    //percentage
    percentage:{
        fontSize:15
    },
    hide:{
        display:'none'
    }

})
