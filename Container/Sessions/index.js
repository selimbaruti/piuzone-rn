import React, {Component} from 'react';
import {View,ActivityIndicator,Text,StyleSheet} from 'react-native';
import styles from './styles'
import Months from './../../Components/Months/index'
import List from './List/index'
import moment from 'moment';
import 'moment/locale/it'
import instance from './../../api/instance'
import RNPickerSelect from 'react-native-picker-select';
import Colors from './../../Constants/Colors'

let list = []

export default class Sessions extends Component {

  constructor(props) {
    super(props);
    this.state = {
      years:null,
      month: moment().format('MM'),
      isLoading:false,
      year: moment().format('YYYY')
     };
   }

   componentDidMount(){
     this.getYears()
   }

   changeMonth(month){
    this.setState({month})
  }

  getYears(){
    list = []
    this.setState({isLoading:true})
    instance.get('telemetria/getTrainingYears')
    .then(response => {
      console.log(response)
      response.data.forEach(data => {
        list.push(
          {
            label:data.toString(),
            value:data.toString()
          }
        )
      })
      this.setState({isLoading:false})
    })
    .catch(error => {
      console.log(error)
    })
  }
  
  years(list){
    if(list.length > 1){
      return(
      <RNPickerSelect
      style={pickerSelectStyles}
      onValueChange={(value) => {
        this.setState({year:value})
      }}
      items={list}
      value={this.state.year}
      placeholder={{}}
      useNativeAndroidPickerStyle={false}
       />
      )
    }
    else{
      return(
        <Text style={styles.year}>{moment().format('YYYY')}</Text>
      )
    }  
  }
  

    render(){  
      console.log(list[0] ? list[0] : '...')
      if(this.state.isLoading){
        return(
          <View style={styles.loader}> 
            <ActivityIndicator size="large" color={Colors.primary}/>
          </View>
        )
      }

      return (
        <View style={styles.container}>
          <View style={{display:'flex'}}>
          {this.years(list)}
          </View>
          <View flex={1}>
          <Months
          month={this.state.month}
          year={this.state.year}
          changeMonth={this.changeMonth.bind(this)}
          />
          </View>
          <View flex={8}>
           <List
           navigation={this.props.navigation}
           month={this.state.month}
           year={this.state.year}
           />
          </View>
        </View>
      )
    }
  }
  

  const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
      fontSize:18, 
      fontFamily:'Montserrat-Medium', 
      color:'#fff', 
      textAlign:'center', 
      padding:'2%',
      marginTop:'3%',
      backgroundColor:Colors.primary,
    },
    inputAndroid: {
      fontSize:18, 
      fontFamily:'Montserrat-Medium', 
      color:'#fff', 
      textAlign:'center', 
      padding:'2%',
      marginTop:'3%',
      backgroundColor:Colors.primary,
    },
  });
  