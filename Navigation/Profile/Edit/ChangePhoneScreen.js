import React, {Component} from 'react';
import {View,StyleSheet,Text} from 'react-native';
import ChangePhone from './../../../Components/ChangePhone/index'
import SafeAreaView from 'react-native-safe-area-view';
export default class ChangePhoneScreen extends Component {
  constructor(props){
    super(props);    
  }

  static navigationOptions = {
    title:'Cellulare',
  };

  render() {
    const { navigation } = this.props;
    const data = navigation.getParam('data', '');

      return (
        <SafeAreaView style={styles.container}>
            <ChangePhone phone={data} navigation={this.props.navigation} />
        </SafeAreaView>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
  });
