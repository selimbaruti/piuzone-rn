import { StyleSheet} from 'react-native';
import Fonts from './../../Common/Fonts'

export default StyleSheet.create({
    container:{
        flex: 1,
    },
    top:{
        flex:4,
    },
    bottom:{
        flex:1,
        justifyContent:'flex-end',
    },
    title:{
        fontFamily: Fonts.MontserratMedium
    }
  });