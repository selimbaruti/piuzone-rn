import React, {Component} from 'react';
import { View, Text} from 'react-native';
import styles from './styles'
import List from './../../Components/List/index'
import { Avatar} from 'react-native-elements'
import { connect } from "react-redux";
import { url } from './../../Constants/url'
import Header from '../../Components/Header';

class Profile extends Component {

  avatar(){
    if(this.props.profilePic == "" || this.props.profilePic == null){
      return(
        <Avatar
        title={this.avatarTitle()}
        size="large"
        rounded
        />
      )
    }
    else{
      return(
      <Avatar
      rounded
      size="large"
      source={{
        // uri: url[this.props.u].image + this.props.profilePic,
      }}
      />   
      )         
    }
  }


    avatarTitle(){
      let {name,surname} = this.props
      if(name && surname){
      let name = this.props.name.charAt(0)
      let surname = this.props.surname.charAt(0)
      return name+surname
      }
    }

    render() {
      return (
        
        <View style={styles.container}>
          
            <View style={styles.header}>
              <View style={{flex:2, marginTop:'4%',marginLeft:'4%',marginRight:'1%'}}>
            {/* <Avatar
            title={this.avatarTitle()}
            size="large"
            rounded
            /> */}
            {this.avatar()}
        </View>
        <View style={{flex:7}}>
        <Text style={{fontFamily:'Montserrat-SemiBold', fontSize:26,lineHeight:30}}>{this.props.name} {this.props.surname}</Text>
            <Text style={{fontFamily:'Montserrat-Medium',color:'#888887',fontSize:15,lineHeight:20}}>{this.props.email}</Text>
        </View>

            </View>
            <View style={styles.body}>
                <List navigation={this.props.navigation} />
            </View>
        </View>
      )
    }
  }
  

  const mapStateToProps = state => {
    return{
        token:state.authReducer.token,
        name: state.userReducer.username,
        surname: state.userReducer.surname,
        phone: state.userReducer.phone,
        email: state.userReducer.email,
        gender: state.userReducer.gender,
        dateofbirth: state.userReducer.dateofbirth,
        profilePic: state.userReducer.profilePic,
        u: state.userReducer.palestre
    }
}

export default connect(mapStateToProps,null)(Profile)