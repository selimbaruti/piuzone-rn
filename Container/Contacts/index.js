import React, {Component} from 'react';
import { View, Text} from 'react-native';
import styles from './styles'
import Header from './../../Components/Header/index'
import ContactModes from './../../Components/ContactModes/index'

export default class Profile extends Component {

    render() {
      return (
        <View style={styles.container}>
            <View style={styles.header}>
            <Header title="Contatti" back one navigation={this.props.navigation} />
            </View>
            <View style={styles.body}>
                <ContactModes />
            </View>
        </View>
      )
    }
  }
  

