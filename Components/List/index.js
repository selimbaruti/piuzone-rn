//profile list
import React, {Component} from 'react';
import { View, Alert } from 'react-native';
import styles from './styles'
import { ListItem } from 'react-native-elements'
import TabBarIcon from './../../Components/TabBarIcon/index'
import Images from './../../Common/Images'
import { connect } from "react-redux";
import { authSetToken,authRemoveToken } from './../../store/actions/auth';
import { bindActionCreators } from 'redux';


class List extends Component {

    logout = () =>{
        this.props.authRemoveToken()
        this.props.navigation.navigate('AuthLoading',{validate:true});
        // this.props.navigation.navigate('AuthLoading');

    }

    render() {
        const list = [
            {
                name: 'I MIEI DATI',
                icon: Images.icons.data,
                routeName: 'MyData'

            },
            {
                name: 'BIOMETRIA',
                icon: Images.icons.biometria,
                routeName: 'Biometrics'
            },
            {
                name: 'CONTATTACI',
                icon: Images.icons.contact,
                routeName: 'Contacts'
            },
        ]
          
      return (
        <View style={styles.container}>
                <View style={styles.top}>
                    {list.map((l, i) => (
                    <ListItem
                    key={i}
                    title={l.name}
                    leftIcon={<TabBarIcon icon={l.icon} />}
                    bottomDivider
                    chevron={<TabBarIcon icon={Images.icons.arrow} />}
                    onPress={() => this.props.navigation.navigate(l.routeName)}
                    titleStyle={styles.title}
                    />
                    ))}
                </View>

                <View style={styles.bottom}>
                    <ListItem
                    title={'ESCI'}
                    leftIcon={<TabBarIcon icon={Images.icons.esc} />}
                    topDivider
                    chevron={<TabBarIcon icon={Images.icons.arrow} />}
                    titleStyle={styles.title}
                    onPress={() => {
                        Alert.alert(
                          'Stai uscendo',
                          'Sei sicuro di voler uscire?',
                          [
                            {
                              text: 'NO',
                              onPress: () => console.log('Cancel Pressed'),
                              style: 'cancel',
                            },
                            {text: 'SI', onPress: () => this.logout()},
                          ],
                          {cancelable: false},
                        );
                      }
                    }      
                    />
                </View>
        </View>
      )
    }
  }

  const mapStateToProps = state => {
    return{
        token:state.authReducer.token,
        user: state.userReducer.user,
        profilePic: state.userReducer.profilePic
    }
}

const mapDispachToProps = dispatch => {
    return bindActionCreators({
        authSetToken,
        authRemoveToken
   },dispatch)
}

export default connect(mapStateToProps,mapDispachToProps)(List)
  

 