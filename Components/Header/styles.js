import { StyleSheet, Platform, Dimensions } from 'react-native'
import { grey } from 'ansi-colors'

const { width } = Dimensions.get('window')

export default StyleSheet.create({
    container: {
        flex:3,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    title:{
        fontFamily:'Montserrat-SemiBold', 
        fontSize:32,
        textAlign:'center', 
    },
    title2:{
        fontFamily:'Montserrat-SemiBold', 
        fontSize:25,
        lineHeight:30,
        textAlign:'center', 
    },
    hide:{
        display: 'none'
    }
    
})
