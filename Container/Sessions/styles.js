import { StyleSheet } from 'react-native'
import Colors from './../../Constants/Colors'
import Fonts from './../../Common/Fonts'

export default StyleSheet.create({
    container: {
        flex:1,
    },
    year:{
        fontSize:18, 
        fontFamily:'Montserrat-Medium', 
        color:'#888887', 
        padding:'1%'
    },
    loader:{
        marginTop:'4%'
    },
    year:{
        fontSize:18, 
        fontFamily: Fonts.MontserratMedium, 
        color:'#fff', 
        textAlign:'center', 
        padding:'2%',
        marginTop:'3%',
        backgroundColor: Colors.primary
    }
})
