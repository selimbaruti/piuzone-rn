import React, {Component} from 'react';
import {View,StyleSheet,Text} from 'react-native';
import Biometric from './../../Container/Biometric/index'
import SafeAreaView from 'react-native-safe-area-view';
export default class BiometricsScreen extends Component {

  constructor(props){
    super(props);    
  }


  static navigationOptions = {
    header:null,
  };

  render() {
      return (
        <SafeAreaView style={styles.container}>
            <Biometric navigation={this.props.navigation} />
        </SafeAreaView>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
  });
