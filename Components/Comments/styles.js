import { StyleSheet} from 'react-native';
import Colors from './../../Constants/Colors'

export default StyleSheet.create({
    bcontainer:{
        alignItems:'center', 
    },
    button:{
        width:'100%', 
        backgroundColor: Colors.primary, 
        display: 'flex', 
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text:{
        fontSize: 20,
        color: '#FFFFFF'
    },
    spinnerTextStyle:{
        color:"#f3f3f3",
        fontSize:15     
    }
  });