import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    icon: {
      width: 40,
      height: 40,
      resizeMode: 'contain',
    }
})

