import React from 'react'
import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import {createBottomTabNavigator} from 'react-navigation-tabs'
import HomeScreen from './HomeScreen'
import TelemetryScreen from './Telemetry/TelemetryScreen'
import TrainingDetailsScreen from './Telemetry/TrainingDetailsScreen'
import ProfileScreen from './Profile/ProfileScreen'
import ContactsScreen from './Profile/ContactsScreen'
import BiometricsScreen from './Profile/BiometricsScreen'
import MyDataScreen from './Profile/MyDataScreen'
import ChangeNameScreen from './Profile/Edit/ChangeNameScreen'
import ChangeSurnameScreen from './Profile/Edit/ChangeSurnameScreen'
import ChangePhoneScreen from './Profile/Edit/ChangePhoneScreen'
import ChangeEmailScreen from './Profile/Edit/ChangeEmailScreen'
import ChangeGenderScreen from './Profile/Edit/ChangeGenderScreen'
import ChangeDateOfBirthScreen from './Profile/Edit/ChangeDateOfBirthScreen'
import TabBarIcon from './../Components/TabBarIcon/index'
import Images from './../Common/Images'
import Colors from '../Constants/Colors'
import ChangeNicknameScreen from './Profile/Edit/ChangeNicknameScreen'

//home stack
const HomeStack = createStackNavigator(
    {
      Home: { screen: HomeScreen},
    },
    {
      defaultNavigationOptions: {
        header: null
      },
    }
  );
  
  const TelemetryStack = createStackNavigator(
    {
      Telemetry: { screen: TelemetryScreen},
      TrainingDetails: {screen:TrainingDetailsScreen}
    },
    {
      defaultNavigationOptions: {
        header: null
      },
    }
  );
  
  const ProfileStack = createStackNavigator(
    {
      Profile: {screen: ProfileScreen},
      Contacts: {screen: ContactsScreen},
      Biometrics: {screen: BiometricsScreen},
      MyData : {screen: MyDataScreen},
      ChangeName: {screen:ChangeNameScreen},
      ChangeNickname: {screen:ChangeNicknameScreen},
      ChangeSurname: {screen: ChangeSurnameScreen},
      ChangePhone: {screen: ChangePhoneScreen},
      ChangeEmail: {screen: ChangeEmailScreen},
      ChangeGender: {screen:ChangeGenderScreen},
      ChangeDateOfBirth: {screen:ChangeDateOfBirthScreen}
      
    },
    {
      defaultNavigationOptions: {
        header:null
      },
    }
  )
  

  const AppNavigator = createBottomTabNavigator(
    {
      //home tab
      home: {
        screen: HomeStack,
        navigationOptions: {
          tabBarLabel: 'Home',
          tabBarIcon: ({ tintColor }) => (
            <TabBarIcon icon={Images.icons.home} tintColor={tintColor} />
          ),
        },
      },
    
      //telemetry tab
      telemetry: {
        screen: TelemetryStack,
        navigationOptions: {
          tabBarLabel: 'Allenamenti',
          tabBarIcon: ({ tintColor }) => (
            <TabBarIcon icon={Images.icons.telemetry} tintColor={tintColor} />
          ),
        },
      },
  
      //profile tab
      profile: {
        screen: ProfileStack,
        navigationOptions: {
          tabBarLabel: 'Profilo',
          tabBarIcon: ({ tintColor }) => (
            <TabBarIcon icon={Images.icons.profile} tintColor={tintColor} />
          ),
        },
      }
    },
  
      //navigator options
      {
        initialRouteName: 'home',
        tabBarOptions: {
          showIcon: true,
          showLabel: true,
          activeTintColor: Colors.primary,
          inactiveTintColor: Colors.secondary,
          keyboardHidesTabBar:false
        },
      }
  )
  
  export default createAppContainer(AppNavigator)