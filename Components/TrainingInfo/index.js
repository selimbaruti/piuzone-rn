import React, {Component} from 'react';
import { Text, View, Image,TouchableOpacity} from 'react-native';
import styles from './styles'
import Colors from './../../Constants/Colors'
import Popover from 'react-native-popover-view'



export default class TrainingInfo extends Component {

  state = {
    isVisible: false
  }

  showPopover() {
    this.setState({isVisible: true});
  }

  closePopover() {
    this.setState({isVisible: false});
  }


  render() {
    const {icon, info, description, time,big} = this.props;
        return (
          <View style={styles.container}>

                  <Image style={big ? styles.iconSmall : styles.icon} source={{ uri: icon}} />
                  {/* <TouchableOpacity ref={ref => this.touchable = ref} style={styles.button} onPress={() => this.showPopover()}> */}
                  <Text style={big ? styles.infoBig : styles.info}>
                      {info}{this.displayPercentage()}
                  </Text>
                  {/* </TouchableOpacity>
                  <Popover
          isVisible={this.state.isVisible}
          fromView={this.touchable}
          popoverStyle={{backgroundColor:Colors.primary, padding:10}}
          onRequestClose={() => this.closePopover()}>
          <Text style={{color:'#fff'}}>5458945393839389539859</Text>
        </Popover> */}

                  <Text style={big ? styles.descriptionBig : styles.description}>{description}</Text>
                  <Text style={big ? styles.timeBig : styles.time }>{time}</Text>
          </View>
        )
    }

    displayPercentage(){
      if(this.props.percentage){
        return <Text style={styles.percentage}>%</Text>
      }
    }
  }
  