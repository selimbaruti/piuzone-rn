import React, {Component} from 'react';
import {View, Text} from 'react-native';
import styles from './styles';
import Data from './../../Components/Data/index';
import {Avatar} from 'react-native-elements';
import Colors from './../../Constants/Colors';
import ImagePicker from 'react-native-image-picker';
import {connect} from 'react-redux';
import instance from './../../api/instance';
import {setProfilePhoto} from './../../store/actions/auth';
import {bindActionCreators} from 'redux';
import Spinner from 'react-native-loading-spinner-overlay';
import { url } from './../../Constants/url';
import Header from '../../Components/Header';
import { showMessage, hideMessage } from "react-native-flash-message";

class MyData extends Component {
  state = {
    isLoading: false,
  };

  handleChoosePhoto = () => {
    const options = {
      noData: true,
    };

    ImagePicker.showImagePicker(options, response => {

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};
        let data = new FormData();
        if(response.fileSize <= 8388608){
          this.setState({isLoading: true});
          let i = {
            uri:
              Platform.OS === 'android'
                ? source.uri
                : source.uri.replace('file://', ''),
            type: 'multipart/form-data',
            name: source.uri.split('/').pop(),
          };
          data.append('photo', i);
  
          instance
            .post('user/update/profilePhoto', data, {
              headers: {
                'Content-Type': 'multipart/form-data',
              },
            })
            .then(response => {
              this.props.setProfilePhoto(response.data.img_src);
              this.setState({isLoading: false});
            })
            .catch(err => {
              this.setState({isLoading: false});
            });
          }
        else{
          showMessage({
            message: "Il file allegato non deve superare i 8MB",
            type: "warning",
          })
      }
    }
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
        <Header title="I miei dati" back one navigation={this.props.navigation} />
        </View>
        <Spinner
          visible={this.state.isLoading}
          textContent={'Salvando...'}
          color={'#f3f3f3'}
          textStyle={styles.spinnerTextStyle}
        />
        <View
          style={{
            alignItems: 'center',
            flexDirection: 'column',
          }}>
          <View
            style={{
              marginTop: '4%',
              marginLeft: '4%',
              marginBottom: '4%',
              marginRight: '1%',
            }}>
            <Avatar
              onEditPress={this.handleChoosePhoto}
              source={{
                uri: url[this.props.u].image + this.props.profilePic,
              }}
              size="large"
              rounded
              showEditButton
              editButton={{
                name: 'add',
                type: 'material',
                backgroundColor: Colors.primary,
                color: Colors.secondary,
                underlayColor: '#fff',
              }}
            />
          </View>
          <View style={{marginBottom: '4%'}}>
            <Text
              style={{
                fontFamily: 'Montserrat-SemiBold',
                color: Colors.primary,
                fontSize: 14,
              }}>
              MODIFICA IMMAGINE
            </Text>
          </View>
        </View>
        <View style={styles.body}>
          <Text
            style={{
              paddingLeft: 16,
              fontFamily: 'Montserrat-SemiBold',
              color: Colors.primary,
              fontSize: 14,
            }}>
            DATI PERSONALI
          </Text>
          <Data navigation={this.props.navigation} />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    profilePic: state.userReducer.profilePic,
    u: state.userReducer.palestre
  };
};

const mapDispachToProps = dispatch => {
  return bindActionCreators(
    {
      setProfilePhoto,
    },
    dispatch,
  );
};

export default connect(mapStateToProps, mapDispachToProps)(MyData);
