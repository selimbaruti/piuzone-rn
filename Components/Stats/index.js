import React, {Component} from 'react';
import { Text, View} from 'react-native';
import styles from './styles'

export default class Stats extends Component {
    
    render() {
        const {title,percentage,time,color} = this.props;
        return (
          <View style={styles.container}>
            <View style={styles.textbox}>
              <Text style={[styles.title, {color}]}>{title}</Text>
              <Text style={[styles.percentage, {color}]}>
              {percentage}<Text style={{fontSize:12}}>%</Text>
              </Text>
            </View>
            <View style={styles.bar}>
              <View style={{width:'90%', backgroundColor:'#3d3d3d', height:12}}>
                     <View style={{flex:1,backgroundColor:color, width: percentage+'%'}}></View>
              </View>
            </View>
            <Text style={styles.time}>{time}</Text>
          </View>
        )
    }
  }
  