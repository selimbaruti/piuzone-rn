import { StyleSheet, Dimensions } from 'react-native'
import Fonts from './../../Common/Fonts'

export default StyleSheet.create({
    container:{
        marginBottom:'3%'
    },
    textbox:{
        flexDirection:'row', 
        alignSelf:'center',
        width:'90%',
        marginBottom:'1%'
    },
    title: {
        width:'80%', 
        fontSize:17,
        fontFamily: Fonts.MontserratSemiBold
    },
    percentage: {
        marginLeft:'auto',
        fontSize:17,
        fontFamily: Fonts.MontserratSemiBold
    },
    bar: {
        flex:1,
        alignItems: 'center',
    },
    time:{
        alignSelf: 'center', 
        color:'grey',
        fontSize:13,
        fontFamily: Fonts.MontserratSemiBold
    }

})
