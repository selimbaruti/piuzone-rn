import React, {Component} from 'react';
import { StyleSheet,View, Text} from 'react-native';
import Telemetry from './../../Container/Telemetry/index'
import Sessions from './../../Container/Sessions/index'
import Button from './../../Components/Button/index'
import Header from './../../Components/Header/index'
import SafeAreaView from 'react-native-safe-area-view';
export default class TelemetryScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      telemetry:true,
      sessioni:false
    }
  }
  
    telemetry(){
      this.setState({ 
        telemetry: true,
        sessioni: false, 
      })
    }

    sessions(){
      this.setState({
        telemetry:false,
        sessioni:true
      })
    }

    renderButtons(){
      if(this.state.telemetry){
        return(
          <View style={styles.buttons}>
              <Button title="Totali" active changeScreen={this.telemetry.bind(this)}  />
              <Button title="Sessioni" changeScreen={this.sessions.bind(this)} />
          </View>
        ) 
      }
      else{
        return(
          <View style={styles.buttons}>
              <Button title="Totali"  changeScreen={this.telemetry.bind(this)}  />
              <Button title="Sessioni" active changeScreen={this.sessions.bind(this)} />
          </View>    
        )
      }
    }

    renderBody(){
      if(this.state.telemetry){
        return(
          <Telemetry navigation={this.props.navigation} />
        )
      }
      else{
        return(
          <Sessions navigation={this.props.navigation} />
          )
      }
    }


    render() {
      return (
        <SafeAreaView style={styles.container}>
          <View style={styles.header}>
          <Header title="Allenamenti" navigation={this.props.navigation}  />
          {this.renderButtons()}
          </View>
          <View style={styles.body}>
          {this.renderBody()}
          </View>  
        </SafeAreaView>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    header:{
      display:"flex",
      height:140
    },
    body:{
      flex:10
    },
    buttons:{
      flex:1,
      flexDirection: 'row',
      justifyContent: 'center',
      marginBottom: '3%'
    },
  });
