import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack'
import MainNavigator from './index';
import SignInScreen from './Auth/SignInScreen'
import ValidateEmailScreen from './Auth/ValidateEmailScreen'
import CompleteRegistration from './Auth/CompleteRegistration'
import RegisterScreen from './Auth/RegisterScreen'
import LostPasswordScreen from './Auth/LostPasswordScreen'
import ResetPasswordScreen from './Auth/ResetPasswordScreen'
import AuthLoadingScreen from './AuthLoadingScreen'
import NavigationService from './NavigationActions'

const AuthStack = createStackNavigator(
  {
    SignIn: {screen: SignInScreen,path:'signIn'},
    Register: RegisterScreen,
    ValidateEmail: {screen: ValidateEmailScreen, path:'validate-email*'},
    CompleteRegistration: CompleteRegistration,
    LostPassword: LostPasswordScreen,
    ResetPassword: {screen: ResetPasswordScreen, path:'resetPassword*'}
  },
  {
    initialRouteName: "SignIn",
    defaultNavigationOptions: {
      header: null
    },
  }
);



const TheApp = createAppContainer(createSwitchNavigator(


  {
    AuthLoading:  AuthLoadingScreen,
    App: MainNavigator,
    Auth: {screen:AuthStack},
  },
  {
    initialRouteName: 'AuthLoading',
  },
  
));


const uriPrefix = 'https://';


export default MainApp = () => <TheApp uriPrefix={uriPrefix}  ref={navigatorRef => {NavigationService.setTopLevelNavigator(navigatorRef)}} />;

