import React, {Component} from 'react';
import { View,StyleSheet} from 'react-native';
import Profile from './../../Container/Profile/index'
import SafeAreaView from 'react-native-safe-area-view';
export default class ProfileScreen extends Component {

  constructor(props){
    super(props);    
  }


  static navigationOptions = {
    header:null,
  };

  render() {
      return (
        <SafeAreaView style={styles.container}>
            <Profile navigation={this.props.navigation} />
        </SafeAreaView>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
  });
