import { GET_USER_DATA,SET_PROFILE_PHOTO,SET_USER_NAME,SET_USER_SURNAME,SET_USER_PHONE,SET_USER_EMAIL,SET_USER_GENDER,SET_USER_DATEOFBIRTH, SET_USER_POINTS,SET_USER_NICKNAME, SET_USER_PALESTRE ,REFRESH_USER_SESSIONS } from "../actions/actionTypes";

const initialState = {
  profilePic: null,
  username: null,
  nickname:null,
  surname: null,
  phone:null,
  email:null,
  gender:null,
  dateofbirth: null,
  points:null,
  refreshUS:false,
  palestre:null
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER_DATA:
      return {
        ...state,
        user: action.user
      };
    case SET_PROFILE_PHOTO:
        return {
          ...state,
          profilePic: action.img_src
    };
    case SET_USER_NAME:
        return {
          ...state,
          username: action.name
    };
    case SET_USER_SURNAME:
        return {
          ...state,
          surname: action.surname
    };
    case SET_USER_PHONE:
        return {
          ...state,
          phone: action.phone
    };
    case SET_USER_EMAIL:
        return {
          ...state,
          email: action.email
    };
    case SET_USER_GENDER:
        return {
          ...state,
          gender: action.gender
    };
    case SET_USER_DATEOFBIRTH:
        return {
          ...state,
          dateofbirth: action.dateofbirth
    };
    case SET_USER_POINTS:
      return {
        ...state,
        points: action.points
    };
    case SET_USER_NICKNAME:
      return {
        ...state,
        nickname:action.nickname
    };
    case SET_USER_PALESTRE:
      return {
          ...state,
          palestre:action.palestre
    }  
    case REFRESH_USER_SESSIONS:
      return{
        ...state,
        refreshUS: action.refresh
      }  
    default:
      return state;
  }
};

export default userReducer;