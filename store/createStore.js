import AsyncStorage from '@react-native-community/async-storage';
import { applyMiddleware,createStore } from 'redux';
import reducers from './reducers/index'
import { persistReducer } from 'redux-persist';
import thunk from 'redux-thunk'
import logger from 'redux-logger'

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    blacklist: ['navigation','uiReducer'],
    }
    
    const persistedReducer = persistReducer(persistConfig, reducers)
    let store = createStore(persistedReducer,applyMiddleware(thunk,logger))
    
export default store