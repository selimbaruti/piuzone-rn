import { StyleSheet} from 'react-native';
import Colors from './../../Constants/Colors'
import Fonts from './../../Common/Fonts'

export default StyleSheet.create({
    container:{
        flex: 1,
    },
    header:{
        flex:1,
        paddingTop:'4%',
        paddingBottom:'2%'
    },
    body:{
        flex: 10
    },
    top:{
        flex:4,
    },
    bottom:{
        flex:1,
        justifyContent:'flex-end',
    },
    icontainer:{
        paddingLeft:'2%',
        paddingRight:'2%',
        paddingTop:'2%'
    },
    info:{
        fontSize:16,
        marginBottom:10,
        color:'#8b8d8b',
        fontFamily: Fonts.MontserratMedium

    },
    input: {
        width:'100%',
        fontSize:17,
        height: 50,
        backgroundColor: Colors.background,
        color:"#000",
        borderBottomWidth:2,
        borderBottomColor: Colors.primary ,
        paddingHorizontal:10,
        marginBottom:20,
        fontFamily: Fonts.MontserratMedium
      },
      bcontainer:{
        alignItems:'center', 
    },
    button:{
        width:'100%', 
        backgroundColor: Colors.primary, 
        display: 'flex', 
        height: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text:{
        fontSize: 20,
        color: '#FFFFFF',
        fontFamily: Fonts.MontserratMedium
    },
    spinnerTextStyle:{
        color:"#f3f3f3",
        fontSize:15     
    }
  });