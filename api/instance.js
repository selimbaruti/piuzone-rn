
import axios from 'axios';
import store from './../store/createStore';
import { authSetToken,authRemoveToken } from './../store/actions/auth';
import NavigationService from './../Navigation/NavigationActions'
import { url } from './../Constants/url'

const URLI = url[store.getState().userReducer.palestre] ? url[store.getState().userReducer.palestre].base : 'https://piuzone.centrisportpiu.it'
// const URL="http://192.168.0.222"
const BASE_PATH = '/api/'

let instance = axios.create({
  baseURL: URLI + BASE_PATH,
});

async function refresh(){

    //todo
    //kontrollojm nqs eshte brenda dy javeve
    // if()
    // return true
    // return false
    return axios.get(URLI + BASE_PATH + 'app/auth/refresh', {
        headers:{
            Authorization : `Bearer ${store.getState().authReducer.token}`
        } 
    }).then(response => {
        console.log(response)
            let token = response.headers.authorization.slice(7)
            store.dispatch(authSetToken(token))
            return true
    })
    .catch(error => {
        console.log(error.response)
        store.dispatch(authRemoveToken())
        NavigationService.navigate('AuthLoading',{validate:true});    
        return false
    })
}

instance.interceptors.request.use(request => {
    request.headers.Authorization = "Bearer " + store.getState().authReducer.token
    return request
});

instance.interceptors.response.use(response => {
    return response
}, async error => {
    let originalRequest = error.config
    let retry = false
    if(error.response.status === 401 && !retry){
        retry = true
        let val = await refresh()
        if(val){
        console.log('vali',val)
        originalRequest.headers['Authorization'] = 'Bearer ' + store.getState().authReducer.token;
        return axios(originalRequest)
        }
    }
    return Promise.reject(error)
});

export default instance;

