import React, {Component} from 'react';
import {View,StyleSheet} from 'react-native';
import ChangeName from './../../../Components/ChangeName/index'
import SafeAreaView from 'react-native-safe-area-view';
import ChangeNickname from './../../../Components/ChangeNickname/index'
export default class ChangeNicknameScreen extends Component {
  constructor(props){
    super(props);    
  }


  render() {
    const { navigation } = this.props;
    const data = navigation.getParam('data', '');
      return (
        <SafeAreaView style={styles.container}>
            <ChangeNickname nickname={data} navigation={this.props.navigation} />
        </SafeAreaView>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
  });
