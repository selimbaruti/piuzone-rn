import React, {Component} from 'react';
import {View,ActivityIndicator,Text,ScrollView,Image} from 'react-native';
import { ListItem } from 'react-native-elements'
import styles from './styles'
import TabBarIcon from './../../../Components/TabBarIcon/index'
import Images from './../../../Common/Images'
import Months from './../../../Components/Months/index'
import moment from 'moment';
import 'moment/locale/it'
import instance from './../../../api/instance'
import Colors from './../../../Constants/Colors'
import { connect } from "react-redux";


let list = []
// let list = [{date:'12/06/2019' , duration:'01:44:42'},{date:'14/06/2019' , duration:'00:30:34'}]


class List extends Component {

  constructor(props) {
    super(props);
    this.state = {
        loading: true,
     };
   }

    componentDidMount(){
      this.getSessions()
    }

    componentDidUpdate(prevProps){
      if(prevProps.month != this.props.month || prevProps.year != this.props.year || prevProps.refresh != this.props.refresh){
        this.getSessions()
      }
    }

    getSessions(){
      this.setState({loading:true})
        instance.get('telemetria/trainings/' + this.props.month + '/' + this.props.year)
          .then(response => {
            this.list(response.data)
          })
          .catch(error =>
            console.log(error)
          ) 
        }
    
    list(data){
      list = []
      data.trainings.forEach(data => {
        list.push({date: moment(data.start).format('DD/MM/YYYY'), duration: data.duration,training_id: data.training_id,comments: data.total_comments })
      })
      this.setState({loading:false})
    }
  
    renderBody(){
      if(list.length == 0){
        return(
          <Text style={styles.message}>NESSUN ALLENAMENTO REGISTRATO</Text>
        )
      }
      else{
        return (
          <ScrollView>
            {list.map((l, i) => (
                <ListItem
                key={i}
                title={l.date}
                subtitle={"(" + l.duration + ")"}
                rightTitle={<View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                 <Image source={{uri: Images.icons.comments}} style={{width:17, height:17, marginRight:3}} />
                    <Text style={{fontSize:18,fontFamily:'Montserrat-Medium' ,lineHeight:18, paddingRight:1}}>{l.comments}</Text>
                  </View> }
                topDivider
                bottomDivider
                onPress={() => this.props.navigation.navigate('TrainingDetails', {
                  training_id: l.training_id,
                  date: l.date
                })}
                titleStyle={{fontFamily:'Montserrat-Medium'}}
                subtitleStyle={{fontFamily:'Montserrat-Medium'}}
                chevron={<TabBarIcon icon={Images.icons.arrow} />}
                contentContainerStyle={{ backgroundColor: 'transparent' }}
              />
            ))
            }
            </ScrollView>
        )
      }
    }

    render() {  
      if(this.state.loading){
        return( 
          <View style={styles.loader}> 
            <ActivityIndicator size="large" color={Colors.primary}/>
          </View>
      )}


      return (
        <View style={styles.container}>
          {this.renderBody()}
          </View>
      )
    }
  }
  
  const mapStateToProps = state => {
    return{
        refresh: state.userReducer.refreshUS,
    }
}

export default connect(mapStateToProps,null)(List)
