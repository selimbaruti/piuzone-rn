import React, {Component} from 'react';
import {View,StyleSheet} from 'react-native';
import ChangeName from './../../../Components/ChangeName/index'
import SafeAreaView from 'react-native-safe-area-view';
export default class ChangeNameScreen extends Component {
  constructor(props){
    super(props);    
  }

  static navigationOptions = {
    title:'Nome',
  };

  render() {
    const { navigation } = this.props;
    const data = navigation.getParam('data', '');

      return (
        <SafeAreaView style={styles.container}>
            <ChangeName name={data} navigation={this.props.navigation} />
        </SafeAreaView>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
  });
