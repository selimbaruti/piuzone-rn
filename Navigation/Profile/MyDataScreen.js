import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import MyData from './../../Container/MyData/index';
import SafeAreaView from 'react-native-safe-area-view';


export default class MyDataScreen extends Component {

  constructor(props){
    super(props);    
  }


  static navigationOptions = {
    header:null,
  };

  render() {
      return (
        <SafeAreaView style={styles.container}>
            <MyData navigation={this.props.navigation} />
        </SafeAreaView>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
  });
