import { StyleSheet} from 'react-native';
import  Fonts from './../../Common/Fonts'

export default StyleSheet.create({
    container:{
        flex: 1,
    },
    title:{
        fontFamily: Fonts.MontserratMedium
    }
  });