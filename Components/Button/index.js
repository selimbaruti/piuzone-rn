import React, {Component} from 'react';
import { StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from './styles';


export default class Button extends Component {
     
    render() {
      const {title, active, changeScreen} = this.props


      return (
          <TouchableOpacity 
          onPress={() => {
            changeScreen();
          }}
          style={ active ? styles.active : styles.button }>
              <Text style={[styles.title]}>{title}</Text>
          </TouchableOpacity>
      )
    }
  }
  