//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet,ActivityIndicator,Image } from 'react-native';
import axios from 'axios';
import Spinner from 'react-native-loading-spinner-overlay';
import url from './../../Constants/url'
import { showMessage, hideMessage } from "react-native-flash-message";

// create a component
class ValidateEmailScreen extends Component {

    static navigationOptions = {
        header: null,
    };

    constructor(props){
        super(props);
        console.log(props)
        this.state = {
            isLoading:true,
            token:props.navigation.state.params? props.navigation.state.params.token?props.navigation.state.params.token:props.navigation.state.params.t: null,
            testo:'Verificando la mail...'
        }
    }

    render() {
        return (
              <Spinner
              visible={this.state.isLoading}
              textContent={'Verificando la mail...'}
              color={"#f3f3f3"}
              textStyle={styles.spinnerTextStyle}
              />
        );
    }

    componentDidMount(){
      axios.post("https://piuzone.centrisportpiu.it/" + "api/auth/register/checkTokenValidity",{
          token: this.state.token,
      })
      .then(resp=>{
        this.setState({isLoading:false})
        this.props.navigation.navigate('CompleteRegistration', {
          token: this.state.token
        })

      })
      .catch(e => {
        console.log(e.response)
        this.setState({isLoading:false})
        showMessage({
          message:e.response ? e.response.data.msg : 'Si è verificato un errore',
          type: "danger",
        });

        this.props.navigation.navigate('AuthLoading',{validate:true});
      });
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    spinnerTextStyle:{
      color:"#f3f3f3",
      fontSize:15     
  }
});

//make this component available to the app
export default ValidateEmailScreen;
