import {combineReducers} from 'redux';
import authReducer from './auth';
import uiReducer from './ui';
import userReducer from './user'

export default combineReducers({
    authReducer,
    uiReducer,
    userReducer,
})