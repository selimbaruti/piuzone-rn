//profile list
import React, {Component} from 'react';
import { View,Linking,Text, TouchableOpacity,ActivityIndicator,TextInput } from 'react-native';
import styles from './styles'
import { ListItem } from 'react-native-elements'
import Spinner from 'react-native-loading-spinner-overlay';
import TabBarIcon from './../../Components/TabBarIcon/index'
import Images from './../../Common/Images'
import instance from './../../api/instance'
import moment from 'moment'
import Colors from './../../Constants/Colors'
import { showMessage, hideMessage } from "react-native-flash-message";
import { connect } from "react-redux";
import { refreshUS } from "./../../store/actions/auth";
import { bindActionCreators } from "redux";



let list = []

class Comments extends Component {

        state = {
            isLoading:true,
            message:'',
            refresh:false,
            save:false
        }

    componentDidMount(){
        this.getComments()
    }

    componentDidUpdate(prevProps){
        if(prevProps.refresh != this.props.refresh){
            this.getComments()
        }
    }

    getComments(){
        list = []
          instance.get('telemetria/getTrainingComments/' + this.props.training_id)
            .then(response => {
                list = response.data.comments
              this.setState({isLoading:false})
            })
            .catch(error =>
              console.log(error)
            )
          }

    comment(date,h,is_client){
        if(is_client == 1){
            return date + " - " + h +  ", Tu:"
        }
        else{
            return date + " - " + h +  ", Trainer:"
        }
    }

    deleteComment(id){
        this.setState({isLoading:true, refresh:true})
        instance.delete('telemetria/deleteTrainingComment/' + id)
        .then(response => {
            showMessage({
              message:response.data.message,
              type: "success",
            });
            this.props.changeRefresh(!this.props.refresh)
            this.props.refreshUS(!this.props.refreshSessions)
            this.setState({isLoading:false})
        })
        .catch(error => {
            console.log(error)
            showMessage({
              message:error.response ? error.response.data.msg : 'Si è verificato un errore',
              type: "danger",
            });
        })
    }

    save(){
        this.setState({save:true})
        instance.post('telemetria/addTrainingComment',{
            comment: this.state.message,
            training_id:this.props.training_id
        })
        .then(response =>{
            showMessage({
              message:response.data.message,
              type: "success",
            });
            this.setState({message:'',save:false})
            this.props.changeRefresh(!this.props.refresh)
            this.props.refreshUS(!this.props.refreshSessions)
        })
        .catch(error => {
          showMessage({
            message:error.response ? error.response.data.msg : 'Si è verificato un errore',
            type: "danger",
          });
        })
    }

    delete(is_client,id){
        if(is_client == 1){
        return(
        <TouchableOpacity
        onPress={() => this.deleteComment(id)}
        >
            <TabBarIcon icon={Images.icons.delete} />
        </TouchableOpacity>
        )
        }
    }

    render() {
        if(this.state.isLoading){
            return(
              <View>
                {/* <ActivityIndicator size="large" color={Colors.primary} /> */}
              </View>
          )}

      return (
        <View style={styles.container}>
        <Spinner
            visible={this.state.save}
            textContent={'Salvando...'}
            color={"#f3f3f3"}
            textStyle={styles.spinnerTextStyle}
        />
          <Text
            style={{
              paddingLeft: 15,
              paddingBottom: 5,
              fontFamily: 'Montserrat-SemiBold',
              color: Colors.primary,
              fontSize: 14,
            }}>
            COMMENTI ({list.length}):
          </Text>
            {list.map((l, i) => (
            <ListItem
            key={i}
            subtitle={l.comment}
            subtitleStyle={{fontSize:16, color:'black',fontFamily: 'Montserrat-Medium'}}
            titleStyle={{fontSize:13,color:'grey',fontFamily: 'Montserrat-Medium'}}
            title={this.comment(moment(l.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD/MM/YYYY'), moment(l.created_at, 'YYYY-MM-DD HH:mm:ss').format('HH:mm') , l.is_client)}
            topDivider
            bottomDivider
            chevron={this.delete(l.is_client, l.id)}
            // onPress={() => this.f(l.id)}
            // titleStyle={styles.title}
            />
            ))}
            <TextInput
            onChangeText={(message) => this.setState({message})}
            value={this.state.message}
            multiline
            returnKeyType="done"
            scrollEnabled={false}
            placeholder="Commenti"
            blurOnSubmit={true}
            style={{padding:8,width:'90%',margin:10,display:'flex', textAlignVertical:'top' ,alignSelf:'center',fontSize:16, height:"auto" , borderColor: Colors.primary, borderWidth:1}}>
            </TextInput>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.button}
              onPress={() => {
                  if(this.state.message != ''){
                  this.save()
                  }
                  else{
                  showMessage({
                    message:'Questo campo non può essere vuoto',
                    type: "warning",
                  }); 
                }               
                }}
              >
                        <Text style={styles.text}>Rispondi</Text>
               </TouchableOpacity>

        </View>
      )
    }
  }


  const mapStateToProps = state => {
    return{
        refreshSessions: state.userReducer.refreshUS,
    }
  }

const mapDispachToProps = dispatch => {
    return bindActionCreators({
      refreshUS
   },dispatch)
}

export default connect(mapStateToProps,mapDispachToProps)(Comments)
 