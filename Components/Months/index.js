import React, { Component } from 'react'
import { View, Animated,StyleSheet,Text ,Dimensions, TouchableOpacity} from 'react-native'
import moment from 'moment';
import 'moment/locale/it'
import Colors from './../../Constants/Colors'
import Fonts from './../../Common/Fonts'

const { width } = Dimensions.get('window')

let calendar  = []
let months = []

export default class Months extends Component {

  componentDidMount(){
    this.timerHandle = setTimeout(() => {this.scrollToB(); this.timerHandle = 0}, 500)
  }

  componentWillUnmount = () => {             
    if (this.timerHandle) {                  
        clearTimeout(this.timerHandle);      
        this.timerHandle = 0;                
    }                                       
  };

   scrollToB = () =>{
    if(moment(this.props.month, 'MM/YYYY').month() > 5){
    this.refs.scrollRef._component.scrollTo({x:width, animated:true})
    }
   }

    color(i){
      if(i == moment().format('MM/YYYY')){
          return Colors.primary
      }
      else{
          return '#3d3d3d'
      }
  }

  border(i){
    // if(i == this.props.months && i != date){
    if(i == this.props.month){
      return styles.border
    }
  }

   
   calendar(){
    calendar = Array(12).fill(0).map((n, i) => moment().month(i).format('MM'))
  }

  

  display(){
    months = [];
      calendar.forEach((data,i) => {
        months.push(
          <View key={i} style={[styles.date, this.border(data)]}>
            <TouchableOpacity onPress={() => {
              this.props.changeMonth(data)
            }}>
            <Text style={{fontSize:17 ,fontFamily:Fonts.MontserratMedium, textAlign:'center', color:this.color(moment(data, 'MM/YYYY').format('MM/YYYY'))}}>{moment(data, 'MM/YYYY').format('MMM').toUpperCase()}</Text>
          </TouchableOpacity>
          </View>
        )
      })
      return months
    }

  render() {

    this.calendar()
    this.display() 
    
    return (
      <View style={styles.container}>
      <Animated.ScrollView 
      horizontal 
      ref="scrollRef"
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{paddingVertical:0}}
      >
      {months}
      </Animated.ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
},
date: {
    width: (width / 6) - (width * 0.02) ,
    marginLeft: width * 0.01,
    marginRight: width * 0.01,
    height:50, 
    justifyContent:'center', 
    alignItems:'center',
},
border:{
    borderBottomWidth:2,
    borderBottomColor: Colors.primary
}
})
