//profile list
import React, {Component} from 'react';
import { ScrollView} from 'react-native';
import styles from './styles';
import { ListItem } from 'react-native-elements';
import moment from "moment";
import "moment/locale/it";
import {connect} from 'react-redux'

class Data extends Component {

    render() {
        const list = [
            {
              title:"Nickname",
              data:this.props.nickname,
              route:"ChangeNickname"
            },
            {
                title: "Nome",
                data: this.props.name,
                route: "ChangeName"
            },
            {
                title: "Cognome",
                data: this.props.surname,
                route: "ChangeSurname"
            },
            {
                title: "Cellulare",
                data: this.props.phone,
                route: "ChangePhone"
            },
            {
                title: "E-mail",
                data: this.props.email,
                route: "ChangeEmail"
            },
            {
                title: "Sesso",
                data: this.props.gender,
                route: "ChangeGender"
            },
            {
                title: "Data di nascita",
                data: this.props.dateofbirth
                ? moment(this.props.dateofbirth, 'YYYY-MM-DD').format("DD/MM/YYYY")
                : "...",      
                route: "ChangeDateOfBirth"
            },
          ];
                
      return (
        <ScrollView>
                {list.map((l, i) => (
                <ListItem
                key={i}
                title={l.title}
                rightTitle={l.data ? l.data : '...'}
                onPress={() => this.props.navigation.navigate(l.route,{
                    data: l.data
                })}
                titleStyle={styles.title}
                rightTitleStyle={styles.righttitle}
                bottomDivider
                />
                ))}
        </ScrollView>
      )
    }
  }
  

  const mapStateToProps = state => {
    return {
      name: state.userReducer.username,
      surname: state.userReducer.surname,
      nickname: state.userReducer.nickname,
      phone: state.userReducer.phone,
      email: state.userReducer.email,
      gender: state.userReducer.gender,
      dateofbirth: state.userReducer.dateofbirth,

    }
  };
  
  
  export default connect(mapStateToProps,null)(Data);
  