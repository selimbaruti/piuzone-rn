import React, {Component} from 'react';
import { View,TextInput,TouchableOpacity,Text,KeyboardAvoidingView} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import styles from './styles'
import instance from './../../api/instance'
import { connect } from "react-redux";
import { setUserNickname } from "./../../store/actions/auth";
import { bindActionCreators } from "redux";
import Header from './../Header/index'
import { showMessage, hideMessage } from "react-native-flash-message";


class ChangeNickname extends Component {

        state = {
            isLoading:null,
            nickname: this.props.nickname
        }

    send(){
        this.setState({isLoading:true})
        instance.post('user/update', {
            nickname: this.state.nickname
        })
        .then(response => {
            this.props.setUserNickname(this.state.nickname)
            this.setState({isLoading:false})
            this.props.navigation.goBack()
        })
        .catch(err => {
            showMessage({
                message:err.response ? err.response.data.msg : 'La modifica non è andata a buon fine',
                type: "danger",
              });
        })
        // setTimeout(() =>{
        //     this.setState({isLoading:false})
        // },3000);
    }

    render() {
      return (
        <View style={styles.container}>
            {/* <Spinner
            visible={this.state.isLoading}
            textContent={'Salvando...'}
            color={"#f3f3f3"}
            textStyle={styles.spinnerTextStyle}
            /> */}
            <View style={styles.header}>
            <Header title="Nickname" back one navigation={this.props.navigation} />
            </View>
            <View style={styles.body}>
            <View style={styles.top}>
                <View style={styles.icontainer}>
                    <Text style={styles.info}>Modifica il tuo nickname</Text>
                    <TextInput style={styles.input}
                    // placeholder={this.placeholder()}
                    autoCorrect={false}
                    value={this.state.nickname}
                    onChangeText={(nickname) => this.setState({ nickname })}
                    />
                </View>
            </View>
            <View style={styles.bottom}>
                <View style={[styles.bcontainer]}>
                    <TouchableOpacity 
                    activeOpacity={0.8}
                    onPress={() => {
                        if(this.state.nickname != ''){
                            this.send()
                            }
                        else{
                            showMessage({
                                message:'Questo campo non può essere vuoto',
                                type: "warning",
                            });                
                        }       
                        }}
                    style={styles.button}>
                        <Text style={styles.text}>Salva</Text>
                    </TouchableOpacity>
                </View>          
            </View>
            </View>
        </View>
      )
    }
  }
  
// const mapStateToProps = state => {
//     return{
//         nickname: state.userReducer.nickname,
//     }
// }

const mapDispachToProps = dispatch => {
    return bindActionCreators({
        setUserNickname
   },dispatch)
}

export default connect(null,mapDispachToProps)(ChangeNickname)