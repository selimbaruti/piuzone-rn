import React, {Component} from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import AppNavigator from './Navigation/AppNavigator'
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import store from './store/createStore'
import { PersistGate } from 'redux-persist/integration/react';
import FlashMessage from "react-native-flash-message";
import {SafeAreaView} from 'react-navigation';

let persistor = persistStore(store)

export default class App extends Component {
  render(){
    return(
      <View style={styles.container}>
        <Provider store={store}>
          <PersistGate persistor={persistor}>  
              <AppNavigator />
              <FlashMessage position="top" />
          </PersistGate>
        </Provider>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
  flex: 1,
  }
})