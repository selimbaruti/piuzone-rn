import { StyleSheet} from 'react-native';
import Colors from './../../Constants/Colors'

export default StyleSheet.create({
    container:{
        flex: 1,
    },
    box:{
        width:'100%', 
        height:100, 
        backgroundColor:Colors.background, 
        borderBottomWidth:1,
        borderBottomColor:Colors.secondary,
        flexDirection:'row'
    },
    top:{
        display:'flex'
    },
    bottom:{
        flex:1,
        justifyContent:'flex-end'
    },
    info:{
        flex:3, 
        justifyContent:'center', 
        paddingLeft:11,
    },
    unit:{
        fontSize:16
    },
    slider:{
        flex:5, 
        alignItems:'center'
    },
    bcontainer:{
        alignItems:'center', 
    },
    button:{
        width:'100%', 
        backgroundColor: Colors.primary, 
        display: 'flex', 
        height: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text:{
        fontSize: 20,
        color: '#FFFFFF'
    },
    spinnerTextStyle:{
        color:"#f3f3f3",
        fontSize:15     
    }
  });