import { StyleSheet} from 'react-native';

export default StyleSheet.create({
    container:{
        flex: 1,
    },
    header:{
        flex: 1,
    },
    body:{
        flex:8,
    },
    spinnerTextStyle:{
        color:"#f3f3f3",
        fontSize:15     
    }
  });