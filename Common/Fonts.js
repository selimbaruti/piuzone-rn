export default Fonts = {
    MontserratRegular: 'Montserrat-Regular',
    MontserratMedium: 'Montserrat-Medium',
    MontserratSemiBold: 'Montserrat-SemiBold'
};