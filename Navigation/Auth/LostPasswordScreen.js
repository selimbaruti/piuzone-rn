import React from 'react'
import { Dimensions,Text,View,TouchableOpacity,ActivityIndicator,Platform,StyleSheet,TextInput,Animated,SafeAreaView,KeyboardAvoidingView,TouchableWithoutFeedback,Keyboard} from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay';
import Colors from './../../Constants/Colors'
import axios from 'axios'
import { showMessage, hideMessage } from "react-native-flash-message";
import { url } from './../../Constants/url';
import RNPickerSelect from 'react-native-picker-select';
import { gyms } from './../../Constants/Gyms'


const windowWidth = Dimensions.get('window').width;


export default class LostPasswordScreen extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            email:'',
            isLoading:false,
            palestre:0
        }
        this.imageWidth = new Animated.Value(windowWidth*0.9);
    }

    keyboardWillShow = (event) => {
      Animated.timing(this.imageWidth, {
        duration: event.duration,
        toValue: windowWidth*0.6,
      }).start();
    };
  
    keyboardWillHide = (event) => {
      Animated.timing(this.imageWidth, {
        duration: event.duration,
        toValue: windowWidth*0.9,
      }).start();
    };
  
      componentWillMount(){
        this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
      }


    recover = () => {
      if(this.state.palestre == 0){
        showMessage({
          message: 'È necessario selezionare prima la palestra alla quale vuole accedere',
          type: "danger",
        })
      }
      else{
      this.setState({isLoading:true})
          axios.post(url[this.state.palestre].base + 'api/auth/recover', {
            email : this.state.email     
        })
        .then(response => {
              this.setState({isLoading:false})
              showMessage({
                message: response.data.msg,
                type: "success",
              })    
              this.props.navigation.navigate('AuthLoading',{validate:true})
        })
        .catch(error => {
          this.setState({isLoading:false})
          showMessage({
            message: error.response ? error.response.data.errors.email : 'Si è verificato un errore',
            type: "danger",
          })
        });
      }
      };


    render(){
      console.log(this.state.palestre)
        // if(this.props.isLoading){
        //     return(
        //         <View style={styles.container}>
        //             <ActivityIndicator size="large" color="#F08019"/>
        //         </View>
        //     )
        // }

    

        return(
            <SafeAreaView style={styles.container}>
            <Spinner
            visible={this.state.isLoading}
            textContent={'Caricando...'}
            color={"#f3f3f3"}
            textStyle={styles.spinnerTextStyle}
            />
            <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null} style={styles.container}>
             <TouchableWithoutFeedback style={styles.container}>
                 <View style={styles.container}>
                    <View  style={styles.logoContainer}>
                      <Animated.Image style={[styles.logo, { width: this.imageWidth }]} source={require('../../assets/images/logo.png')}/>
                    </View>
                    
                    <View style={styles.infoContainer}>
                      <View style={{display:'flex', alignSelf:"center", width:'90%', marginBottom:20}}>
                        <RNPickerSelect
                        style={pickerSelectStyles}
                        onValueChange={(value) => {
                            this.setState({palestre:value})
                        }}    
                          items={gyms}
                          placeholder={{label:"Scegli la tua palestra ...", value:0, color: '#000'}}
                          useNativeAndroidPickerStyle={false}
                          value={this.state.palestre}
                        />
                      </View>
                        <TextInput style={styles.input}
                          placeholder="Email registrata"
                          placeholderTextColor="#000"
                          keyboardType="email-address"
                          autoCorrect={false}
                          returnKeyType="next"
                          autoCapitalize="none"
                         
                          onChangeText={ TextInputValue =>
                            this.setState({email : TextInputValue }) }
                        />
                        <TouchableOpacity style={styles.buttonContainer}  
                        onPress={this.recover}
                        >
                          <Text style={styles.buttonText}>Modifica password</Text>
                        </TouchableOpacity>
                        <View style={styles.signupTextCont}>
                          <Text style={styles.signupText}>Oppure</Text>
                          <TouchableOpacity
                          onPress={() => this.props.navigation.navigate('SignIn')}
                          ><Text style={styles.signupButton}> Entra</Text></TouchableOpacity>
                        </View>                        
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </KeyboardAvoidingView>
              {/* <Overlay
                      isVisible={this.state.isOverlayVisible}
                      overlayStyle={{borderRadius: 0, padding: 0,margin:0,}}
                      onBackdropPress={() => this.setState({ isVisible: false })}
                      width="90%"
                      height={300}
                      title="Prent"
                      borderRadius={0}
                      
                      containerStyle={{borderRadius: 0, padding: 0,margin:0,}}
                      >
                  <View style={{flex:1, flexDirection:"column", alignItems:"center",backgroundColor:Colors.secondary, padding:10}}>
                              
                      <Text style={{fontSize:20, textAlign:"center",color:"#f3f3f3", fontWeight:"600", marginBottom:20}}>La tua mail non è stata verificata!</Text>
                      <Text style={{fontSize:18, textAlign:"center",color:"#f3f3f3", marginBottom:20}}>Vuoi chiedere di nuovo mail di verifica?</Text>
                      <View style={{flexDirection:"row", position:"absolute", bottom:20, width:"100%", justifyContent: 'space-between', }} >
  
                          <TouchableOpacity style={{color:"#f3f3f3", alignSelf:"flex-start"}} onPress={()=>this.setState({isOverlayVisible:false})}>
                              <View style={{borderRadius:1, borderWidth:1, borderColor:"#f3f3f3",paddingHorizontal:20, paddingVertical:10}}>
                                  <Text style={{color:"#f3f3f3", fontSize:20, fontWeight:"600"}}>Annulla</Text>
                              </View>
                          </TouchableOpacity>
                          <TouchableOpacity style={{backgroundColor:"red", alignSelf:"stretch"}} onPress={this.resendVerificationMail}>
                              <View style={{borderRadius:1, borderWidth:1, borderColor:Colors.primary,paddingHorizontal:20, paddingVertical:10}}>
                                  <Text style={{color:"#f3f3f3", fontSize:20, fontWeight:"600"}}>Invia mail</Text>
                              </View>
                          </TouchableOpacity>
  
                      </View>
                  </View>
                </Overlay> */}
            </SafeAreaView>
          )
    }
}

const styles = StyleSheet.create({
    container:{

      flex:1,
      // backgroundColor: Colors.primary,
      backgroundColor: '#ffffff',
    },
    spinnerTextStyle:{

      color:"#f3f3f3",
      fontSize:15
    },  
    logoContainer:{

      alignItems:'center',
      justifyContent:'center',
      flex:1
    },
    logo:{

      flex:1,
      resizeMode:'contain',
      //width:windowWidth*0.7,
      
      //paddingBottom:60
  
    },
    infoContainer:{

      display:'flex',
      alignItems:'center'
    },
    input: {
      width:'90%',
      height: 50,
      backgroundColor: '#f2f3f2',
      // backgroundColor: Colors.secondary,
      color:"#000",
      paddingHorizontal:10,
      marginBottom:20
    },
    buttonContainer:{

        backgroundColor:Colors.primary,
        paddingVertical:15,
        width:'90%'
    },
    buttonText:{

      textAlign:'center',
      fontSize:18,
      color:"#fff",
      fontWeight:"bold"
    },
    signupTextCont : {
      flexGrow: 1,
      alignItems:'flex-end',
      justifyContent :'center',
      paddingVertical:5,
  
      flexDirection:'row'
    },
    signupText: {
      color:'#b1b4b1',
      fontSize:16
    },
    signupButton: {
      color:'#b1b4b1',
      fontSize:16,
      fontWeight:"bold"
    }
 });

 const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize:15, 
    color: '#000',
    backgroundColor:'#f2f3f2',
    height:50,
    paddingHorizontal:10,
  },
  inputAndroid: {
    fontSize:15, 
    color: '#000',
    backgroundColor:'#f2f3f2',
    height:50,
    paddingHorizontal:10,
  },
  placeholder:{
    color:'#000'
  }
  });
