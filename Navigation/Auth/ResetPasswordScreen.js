import React from 'react'
import { Text,View,Alert,TouchableOpacity,Platform,Dimensions,Animated,StyleSheet,TextInput,SafeAreaView,StatusBar,KeyboardAvoidingView,TouchableWithoutFeedback,Keyboard} from 'react-native'
import { connect } from "react-redux";
import {tryAuth} from '../../store/actions/auth';
import { bindActionCreators } from 'redux';
import Spinner from 'react-native-loading-spinner-overlay';
import { showMessage, hideMessage } from "react-native-flash-message";
import axios from 'axios'
import url from './../../Constants/url'
import Colors from './../../Constants/Colors'

const windowWidth = Dimensions.get('window').width;


class ResetPasswordScreen extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            password:'',
            confirmPassword: '',
            isLoading:false
        }
        this.imageWidth = new Animated.Value(windowWidth*0.9);
    }

    static navigationOptions = {
      header: null,
    };


    keyboardWillShow = (event) => {
      Animated.timing(this.imageWidth, {
        duration: event.duration,
        toValue: windowWidth*0.6,
      }).start();
    };
  
    keyboardWillHide = (event) => {
      Animated.timing(this.imageWidth, {
        duration: event.duration,
        toValue: windowWidth*0.9,
      }).start();
    };

    
     
  
      componentWillMount(){
        this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
        
      }
  
      componentWillUnmount() {
       
      }    
  

      complete = () => {
        if(this.state.password == this.state.confirmPassword){
        this.setState({isLoading:true})
        const { navigation } = this.props;
        const token = navigation.getParam('token', null);

        axios.post(url.base +  "api/auth/finishResetPassword",{
            token,
            password: this.state.password
        })
        .then(response => {
          this.setState({isLoading:false})
          showMessage({
            message: response.data.msg,
            type: "success",
        })
          this.props.navigation.navigate('SignIn')
        })  
        .catch(error => {
          console.log(error.response)
          showMessage({
            message: error.response ? error.response.data.msg : 'Si è verificato un errore',
            type: "danger",
        })
        this.setState({isLoading:false})
        this.props.navigation.navigate('AuthLoading',{validate:true});
        })
      }
      else{
        showMessage({
          message: "Le password non corrispondono",
          type: "danger"
        });
      }
    }


    render(){

        return(
            <SafeAreaView style={styles.container}>
              <Spinner
              visible={this.state.isLoading}
              color={"#f3f3f3"}
              />
              <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null} style={styles.container} keyboardVerticalOffset={0}>
              <TouchableWithoutFeedback style={styles.container}>
                  <View style={styles.container}>
                    <View  style={styles.logoContainer}>
                      <Animated.Image style={[styles.logo, { width: this.imageWidth }]} source={require('../../assets/images/logo.png')}/>
                    </View>
                    <View style={styles.infoContainer}>
                        <TextInput style={styles.input}
                          placeholder="Password"
                          placeholderTextColor="#000"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          returnKeyType="next"
                          secureTextEntry
                          onSubmitEditing={()=>this.confirm_pass.focus()}
                          onChangeText={ TextInputValue =>
                            this.setState({password : TextInputValue }) }
                        />
                        <TextInput style={styles.input}
                          placeholder="Conferma password"
                          placeholderTextColor="#000"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          returnKeyType="go"
                          secureTextEntry
                          ref={(input)=>this.confirm_pass = input}
                          onChangeText={ TextInputValue =>
                            this.setState({confirmPassword : TextInputValue }) }
                        />
                        <TouchableOpacity style={styles.buttonContainer}  
                        onPress={() => {
                            if(this.state.password == ''){
                                showMessage({
                                    message:'La password non può essere vuota',
                                    type:'danger'
                                  })                          
                            }
                            else{
                                this.complete()
                            }
                        }}
                        >
                          <Text style={styles.buttonText}>REIMPOSTA PASSWORD</Text>
                        </TouchableOpacity>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </KeyboardAvoidingView>
              {/* <Overlay
                      isVisible={this.state.isOverlayVisible}
                      overlayStyle={{borderRadius: 0, padding: 0,margin:0,}}
                      onBackdropPress={() => this.setState({ isVisible: false })}
                      width="90%"
                      height={300}
                      title="Prent"
                      borderRadius={0}
                      
                      containerStyle={{borderRadius: 0, padding: 0,margin:0,}}
                      >
                  <View style={{flex:1, flexDirection:"column", alignItems:"center",backgroundColor:Colors.secondary, padding:10}}>
                              
                      <Text style={{fontSize:20, textAlign:"center",color:"#f3f3f3", fontWeight:"600", marginBottom:20}}>La tua mail non è stata verificata!</Text>
                      <Text style={{fontSize:18, textAlign:"center",color:"#f3f3f3", marginBottom:20}}>Vuoi chiedere di nuovo mail di verifica?</Text>
                      <View style={{flexDirection:"row", position:"absolute", bottom:20, width:"100%", justifyContent: 'space-between', }} >
  
                          <TouchableOpacity style={{color:"#f3f3f3", alignSelf:"flex-start"}} onPress={()=>this.setState({isOverlayVisible:false})}>
                              <View style={{borderRadius:1, borderWidth:1, borderColor:"#f3f3f3",paddingHorizontal:20, paddingVertical:10}}>
                                  <Text style={{color:"#f3f3f3", fontSize:20, fontWeight:"600"}}>Annulla</Text>
                              </View>
                          </TouchableOpacity>
                          <TouchableOpacity style={{backgroundColor:"red", alignSelf:"stretch"}} onPress={this.resendVerificationMail}>
                              <View style={{borderRadius:1, borderWidth:1, borderColor:Colors.primary,paddingHorizontal:20, paddingVertical:10}}>
                                  <Text style={{color:"#f3f3f3", fontSize:20, fontWeight:"600"}}>Invia mail</Text>
                              </View>
                          </TouchableOpacity>
  
                      </View>
                  </View>
                </Overlay> */}
            </SafeAreaView>
          )
    }
}

const styles = StyleSheet.create({
    container:{
      flex:1,
      // backgroundColor: Colors.primary,
      backgroundColor: '#ffffff',
    },
    spinnerTextStyle:{
      color:"#f3f3f3",
      fontSize:15
    },  
    logoContainer:{
      alignItems:'center',
      justifyContent:'center',
      flex:1,
    },
    logo:{
      flex:1,
      resizeMode:'contain',
      //width:windowWidth*0.7,
      
      //paddingBottom:60
    },
    infoContainer:{
      display:'flex',
      alignItems:'center',
      padding:0
    },
    input: {
      width:'90%',
      height: 50,
      backgroundColor: '#f2f3f2',
      // backgroundColor: Colors.secondary,
      color:"#000",
      paddingHorizontal:10,
      marginBottom:20
    },
    buttonContainer:{
        backgroundColor:Colors.primary,
        paddingVertical:15,
        width:'90%',
        marginBottom:20
    },
    buttonText:{
      textAlign:'center',
      fontSize:18,
      color:"#fff",
      fontWeight:"bold",
    },
    signupTextCont : {
      flexGrow: 1,
      alignItems:'flex-end',
      justifyContent :'center',
      paddingVertical:5,
      
      flexDirection:'row'
    },
    signupText: {
        color:'#b1b4b1',
        fontSize:16
    },
    signupButton: {
        fontSize:16,
        color:'#b1b4b1',
        fontWeight:'500'
    }
 });
  


export default ResetPasswordScreen;