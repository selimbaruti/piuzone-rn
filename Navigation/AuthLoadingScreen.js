// import React from 'react';
// import {ActivityIndicator, View, Linking, Platform} from 'react-native';
// import {connect} from 'react-redux';

// let pattern=/(.+:\/\/)?([^\/]+)(\/.*)*/i;

// class AuthLoadingScreen extends React.Component {
//   constructor(props) {
//     super(props);
//   }

//   componentDidMount() {
//     const { navigation } = this.props;
//     const isValidate = navigation.getParam('validate', null);

//     if(this.props.token || isValidate){
//       this._bootstrapAsync();
//       return;
//     }
//     if (Platform.OS === 'android') {
//       Linking.getInitialURL().then(url => {
//         if (url !== null) {
//           if (url.search('validate-email') > 0) {
//             let arr= pattern.exec(url);
//             let u = arr[1] + arr[2]
//             let token = arr[3].replace(
//               '/validate-email?t=',
//               '',
//             );
//             this.props.navigation.navigate('ValidateEmail', {
//               u:u,
//               token: token,
//             });
//           } else if (url.search('confirm-password') > 0) {
//             let arr= pattern.exec(url);
//             let u = arr[1] + arr[2]
//             let token = arr[3].replace(
//               '/confirm-password?t=',
//               '',
//             );
//             this.props.navigation.navigate('ResetPassword', {
//               u:u,
//               token: token
//             });
//           }
//           else if(url.search('training_id') > 0){
//             let training_id = url.replace(
//               'https://piuzone.centrisportpiu.it/telemetria/?training_id=',
//               '',
//             );
//             this.props.navigation.navigate('TrainingDetails', {
//               training_id:training_id,
//             });
//           }
//         }
//       });
//     } else {
//       Linking.addEventListener('url', event => {
//         if (event.url.search('validate-email') > 0) {
//           let arr= pattern.exec(event.url);
//           let u = arr[1] + arr[2]
//           let token = arr[3].replace(
//             '/validate-email?t=',
//             '',
//           );
//           this.props.navigation.navigate('ValidateEmail', {
//             u:u,
//             token: token,
//           });
//         } else if (event.url.search('confirm-password') > 0) {
//           let arr= pattern.exec(event.url);
//           let u = arr[1] + arr[2]
//           let token = arr[3].replace(
//             'https://piuzone.centrisportpiu.it/confirm-password?t=',
//             '',
//           );
//           this.props.navigation.navigate('ResetPassword', {
//             u:u,
//             token: token
//           });
//         }
//       });
//     }
//     this._bootstrapAsync();
//   }

//   _handleOpenURL(event) {}

//   _bootstrapAsync = async () => {
//     this.props.navigation.navigate(this.props.token ? 'App' : 'Auth');
//   };

//   render() {
//     return (
//       <View>
//         <ActivityIndicator />
//       </View>
//     );
//   }
// }

// const mapStateToProps = state => {
//   return {
//     token: state.authReducer.token,
//   };
// };

// export default connect(mapStateToProps, null)(AuthLoadingScreen);


import React from 'react';
import {ActivityIndicator, View, Linking, Platform} from 'react-native';
import {connect} from 'react-redux';

class AuthLoadingScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { navigation } = this.props;
    const isValidate = navigation.getParam('validate', null);

    if(this.props.token || isValidate){
      this._bootstrapAsync();
      return;
    }
    if (Platform.OS === 'android') {
      Linking.getInitialURL().then(url => {
        if (url !== null) {
          if (url.search('validate-email') > 0) {
            let token = url.replace(
              'https://piuzone.centrisportpiu.it/validate-email?t=',
              '',
            );
            this.props.navigation.navigate('ValidateEmail', {
              token: token,
            });
          } else if (url.search('confirm-password') > 0) {
            let token = url.replace(
              'https://piuzone.centrisportpiu.it/confirm-password?t=',
              '',
            );
            this.props.navigation.navigate('ResetPassword', {token: token});
          }
          // else if(url.search('training_id') > 0){
          //   let training_id = url.replace(
          //     'https://piuzone.centrisportpiu.it/telemetria/?training_id=',
          //     '',
          //   );
          //   this.props.navigation.navigate('TrainingDetails', {
          //     training_id:training_id,
          //   });
          // }
        }
      });
    } else {
      Linking.addEventListener('url', event => {
        if (event.url.search('validate-email') > 0) {
          let token = event.url.replace(
            'https://piuzone.centrisportpiu.it/validate-email?t=',
            '',
          );
          this.props.navigation.navigate('ValidateEmail', {
            token: token,
          });
        } else if (event.url.search('confirm-password') > 0) {
          let token = event.url.replace(
            'https://piuzone.centrisportpiu.it/confirm-password?t=',
            '',
          );
          this.props.navigation.navigate('ResetPassword', {token: token});
        }
      });
    }
    this._bootstrapAsync();
  }

  _handleOpenURL(event) {}

  _bootstrapAsync = async () => {
    this.props.navigation.navigate(this.props.token ? 'App' : 'Auth');
  };

  render() {
    return (
      <View>
        <ActivityIndicator />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.authReducer.token,
  };
};

export default connect(mapStateToProps, null)(AuthLoadingScreen);
