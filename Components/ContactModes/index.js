//profile list
import React, {Component} from 'react';
import { View,Linking } from 'react-native';
import styles from './styles'
import { ListItem } from 'react-native-elements'
import TabBarIcon from './../../Components/TabBarIcon/index'
import Images from './../../Common/Images'

export default class List extends Component {


  reportProblem = () =>{
    this.props.navigation.navigate('ReportProblem')
  }

  tel = () => {
    Linking.openURL('tel:' + '035 270920');
  }

  mail = () =>{
    Linking.openURL('mailto:' + 'area.donna@centrisportpiu.it')
  }

  f = (id) => {
    if(id == 1){
      this.tel()
    }
    else{
        this.mail()
    }
  }


    render() {
        const list = [
            {
                id:1,
                title: 'TELEFONO',
                icon: Images.icons.telephone,
    
            },
            { 
                id:2,
                title: 'E-MAIL',
                icon: Images.icons.email,
            }
          ]
              
      return (
        <View style={styles.container}>
            {list.map((l, i) => (
            <ListItem
            key={i}
            title={l.title}
            leftIcon={<TabBarIcon icon={l.icon} />}
            bottomDivider
            onPress={() => this.f(l.id)}
            titleStyle={styles.title}
            chevron={<TabBarIcon icon={Images.icons.arrow} />}
            />
            ))}
        </View>
      )
    }
  }
  

 