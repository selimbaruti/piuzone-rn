import React, {Component} from 'react';
import { Text, View, TouchableOpacity,Image} from 'react-native';
import {Avatar} from 'react-native-elements' 
import styles from './styles';
import Images from './../../Common/Images'
import { connect } from "react-redux";
import { url } from './../../Constants/url'

class Header extends Component {

  avatar(){
    if(this.props.profilePic == "" || this.props.profilePic == null){
      return(
        <Avatar
        title={this.avatarTitle()}
        size="medium"
        rounded
        onPress={() => this.props.navigation.navigate('Profile')} 
        />
      )
    }
    else{
      return(
      <Avatar
      rounded
      size="medium"
      source={{
        uri: url[this.props.u].image + this.props.profilePic,
      }}
      onPress={() => this.props.navigation.navigate('Profile')} 
      />   
      )         
    }
  }

  avatarTitle(){
    let {name,surname} = this.props
    if(name && surname){
      let name = this.props.name.charAt(0)
      let surname = this.props.surname.charAt(0)
      return name+surname
    }
  }

    renderBackButton(){
      if(this.props.back){
        if(this.props.one){
          return(
          <TouchableOpacity 
          onPress={() => this.props.navigation.goBack()}
          style={styles.bbOne}>
            <Image source={{uri: Images.icons.back}} style={{width:50, height:50}} />
          </TouchableOpacity >
          )
      }
      else if(this.props.two){
        return(
          <TouchableOpacity 
          onPress={() => this.props.navigation.goBack()}
          >
            <Image source={{uri: Images.icons.back}} style={{width:50, height:50}} />
          </TouchableOpacity >
          )
      }
      else if(this.props.backF){
        if(this.props.screen > 1){
        return(
          <TouchableOpacity 
          onPress={() => this.props.changeScreen(this.props.screen - 1)}
          >
            <Image source={{uri: Images.icons.back}} style={{width:50, height:50}} />
          </TouchableOpacity >
        )
    }
  }
}
}

    render(){


      const {title} = this.props

      return (
          <View style={styles.container}>
            <View style={{flex:1}}>
              {this.renderBackButton()}
            </View>
            <View style={{flex:4}}>
              <Text style={styles.title2}>{title}</Text>
            </View>
            <View style={{flex:1}}>
            {/* <Avatar
              rounded
              size="medium"
              title={this.avatarTitle()}
            //   source={{
            //     uri: url + this.state.photo,
            //   }}
              activeOpacity={0.7}
              />             */}
              {this.avatar()}
              </View>
          </View>
      )
    }
}

const mapStateToProps = state => {
  return{
    token:state.authReducer.token,
    name: state.userReducer.username,
    surname: state.userReducer.surname,
    phone: state.userReducer.phone,
    email: state.userReducer.email,
    gender: state.userReducer.gender,
    dateofbirth: state.userReducer.dateofbirth,
    profilePic: state.userReducer.profilePic,
    u: state.userReducer.palestre
}
}

export default connect(mapStateToProps,null)(Header)