import React, {Component} from 'react';
import {View,StyleSheet,Text} from 'react-native';
import ChangeSurname from './../../../Components/ChangeSurname/index'
import SafeAreaView from 'react-native-safe-area-view';
export default class ChangeSurnameScreen extends Component {
  constructor(props){
    super(props);    
  }

  static navigationOptions = {
    title:'Cognome',
  };

  render() {
    const { navigation } = this.props;
    const data = navigation.getParam('data', '');
    
      return (
        <SafeAreaView style={styles.container}>
            <ChangeSurname surname={data} navigation={this.props.navigation} />
        </SafeAreaView>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
  });
