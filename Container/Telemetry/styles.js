import { StyleSheet, Platform, Dimensions } from 'react-native'

export default StyleSheet.create({
    container: {
        flex:1,
    },
    header:{
        flex:2,
    },
    totali:{
        flex:8
    },
    trainingInfoB:{
        flex:3,
        flexDirection:'row',
        marginTop: '3%'  
    },
    traininginfoS: {
        flex:3,
        flexDirection: 'row'
    },
    stats:{
        flex:7,
        marginTop:'2%'
    },
    buttons:{
        flex:1, 
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-end', 
        marginBottom:'4%'
    },
    loader:{
        marginTop:'4%'
    }
})
