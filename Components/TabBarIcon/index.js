
import React from 'react'
import PropTypes from 'prop-types'
import { Image } from 'react-native'
import styles from './styles'


const TabBarIcon = ({ icon, tintColor, css }) => {
  return (
    <Image
      source={{ uri: icon }}
      style={[styles.icon, { tintColor }, css]}
    />
  )
}

TabBarIcon.propTypes = {
  icon: PropTypes.string,
  tintColor: PropTypes.string,
  css: PropTypes.any,
}

export default TabBarIcon
