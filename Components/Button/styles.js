import { StyleSheet } from 'react-native'
import Colors from './../../Constants/Colors'
import Fonts from  './../../Common/Fonts'

export default StyleSheet.create({
    
    container: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginBottom:'4%',
    },
    button: {
        width:120,
        display: 'flex',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft:'2%'
    },
    active:{
        borderColor: Colors.primary,
        borderWidth:2,
        width:120,
        display: 'flex',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft:'2%'
    },
    title: {
        fontSize: 18,
        fontFamily: Fonts.MontserratMedium
    },
})
