import React, {Component} from 'react';
import {View,StyleSheet,Text} from 'react-native';
import ChangeDateOfBirth from './../../../Components/ChangeDateOfBirth/index'
import SafeAreaView from 'react-native-safe-area-view';
export default class ChangeDateOfBirthScreen extends Component {
  constructor(props){
    super(props);    
  }

  static navigationOptions = {
    title:'Data di Nascita',
  };

  render() {
    const { navigation } = this.props;
    const data = navigation.getParam('data', '');

      return (
        <SafeAreaView style={styles.container}>
            <ChangeDateOfBirth dateofbirth={data} navigation={this.props.navigation}/>
        </SafeAreaView>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
  });
