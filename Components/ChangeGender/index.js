import React, {Component} from 'react';
import { View,TextInput,TouchableOpacity,Text} from 'react-native';
import styles from './styles'
import { ListItem } from 'react-native-elements'
import TabBarIcon from './../../Components/TabBarIcon/index'
import Images from './../../Common/Images'
import Spinner from 'react-native-loading-spinner-overlay';
import instance from './../../api/instance'
import { connect } from "react-redux";
import { setUserGender } from "./../../store/actions/auth";
import { bindActionCreators } from "redux";
import Header from './../Header/index'
import { showMessage, hideMessage } from "react-native-flash-message";

class ChangeGender extends Component {
    constructor(props){
        super(props);
        this.state = {
            isLoading:null,
        }
    }

    send(gender){
        this.setState({isLoading:true})
        instance.post('user/update', {
            sesso: gender
        })
        .then(response => {
            this.props.setUserGender(gender)
            this.setState({isLoading:false})
            // this.props.navigation.goBack()
        })
        .catch(err => {
            showMessage({
                message:err.response ? err.response.data.msg : 'La modifica non è andata a buon fine',
                type: "danger",
              });
        })
        // setTimeout(() =>{
        //     this.setState({isLoading:false})
        // },3000);
    }

    check(i){
        if(i == this.props.gender){
            return(
                <TabBarIcon icon={Images.icons.check} />
            )
        }
    }

    render() {
        const list = [
            {
                name: 'Maschio',

            },
            {
                name: 'Femmina',
            },
            {
                name: 'Non Specificato',
            },
        ]

      return (
        <View style={styles.container}>
            <Spinner
            visible={this.state.isLoading}
            textContent={'Salvando...'}
            color={"#f3f3f3"}
            textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.header}>
            <Header title="Sesso" back one navigation={this.props.navigation} />
            </View>
            <View style={styles.body}>
            <View style={styles.top}>
                <View style={styles.icontainer}>
                    <Text style={styles.info}>Modifica sesso</Text>
                    {list.map((l, i) => (
                    <ListItem
                    key={i}
                    title={l.name}
                    bottomDivider
                    topDivider
                    titleStyle={styles.title}
                    chevron={this.check(l.name)}
                    onPress={() => {
                        this.send(l.name)
                    }} />
                    ))}
                </View>
            </View>
            </View>
        </View>
      )
    }
  }
  
const mapStateToProps = state => {
    return{
        gender: state.userReducer.gender,
    }
}

const mapDispachToProps = dispatch => {
    return bindActionCreators({
        setUserGender
   },dispatch)
}

export default connect(mapStateToProps,mapDispachToProps)(ChangeGender)
 