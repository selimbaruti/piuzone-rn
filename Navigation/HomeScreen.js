import React, {Component} from 'react';
import { StyleSheet,View, Text, ImageBackground, Image,TouchableOpacity,Linking} from 'react-native';
import Fonts from './../Common/Fonts'
import { Avatar,Overlay } from 'react-native-elements';
import Colors from '../Constants/Colors';
import Site from './../Components/Site/index'
import { connect } from "react-redux";
import { url } from './../Constants/url'
import {SafeAreaView} from 'react-navigation';
import instance from './../api/instance'
import moment from 'moment'
import Spinner from 'react-native-loading-spinner-overlay';
import { bindActionCreators } from "redux";
import {setProfilePhoto,setUserName,setUserNickname,setUserSurname,setUserPhone,setUserEmail,setUserGender,setUserDateOfBirth,setUserPoints} from './../store/actions/auth'
// import { photos } from './../Common/GymsPhotos'

let gyms = {
  ad: {
      background: require('./../assets/images/areadonna.png')
    },
  cs: {
      background: require('./../assets/images/cenatesotto.png')
  },
  c: {
    background: require('./../assets/images/curno.png')
  }
}

class HomeScreen extends Component {
  
  componentDidMount(){
    if (Platform.OS === 'android') {
      Linking.getInitialURL().then(url => {
        if (url !== null) {
           if(url.search('training_id') > 0){
            let regex = /[?&]([^=#]+)=([^&#]*)/g,
            params = {},
            match
           
            while ((match = regex.exec(url))) {
            params[match[1]] = match[2]
            }
            const { training_id, training_data } = params
            this.props.navigation.navigate('TrainingDetails', {
              training_id:training_id,
              date:moment(training_data, 'YYYY-MM-DD').format('DD/MM/YYYY')
            });
          }
        }
      });
    } else {
      Linking.addEventListener('url', event => {
        
        if (event.url === null)
          return;
          
        if(event.url.search('training_id') > 0){
          
          let url = event.url;
          let regex = /[?&]([^=#]+)=([^&#]*)/g,
            params = {},
            match
          while ((match = regex.exec(url))) {
            params[match[1]] = match[2]
          }
          const { training_id, training_data } = params
          this.props.navigation.navigate('TrainingDetails', {
            training_id:training_id,
            date:moment(training_data, 'YYYY-MM-DD').format('DD/MM/YYYY')
          });
        }
        
      });
    }
  
    //TODO load profile
    instance.get('auth/user')
    .then(response => {
      this.setState({isLoading:false})
      this.props.setProfilePhoto(response.data.data.img_src)
      this.props.setUserName(response.data.data.nome)
      this.props.setUserNickname(response.data.data.nickname)
      this.props.setUserSurname(response.data.data.cognome)
      this.props.setUserPhone(response.data.data.cellulare)
      this.props.setUserEmail(response.data.data.email)
      this.props.setUserGender(response.data.data.sesso)
      this.props.setUserDateOfBirth(response.data.data.nato_il)
      this.props.setUserPoints(response.data.data.points)
    })
    .catch(error => {
      console.log(error.response)
    })
  }

  state={
    isVisible:false,
    isLoading:true,
  }


  k(points){
    if(points >= 1000000000){
      return Math.floor(points/1000000000) + "B"
    }
    else if(points >= 1000000){
      return Math.floor(points/1000000) + "M"
    }
    else if(points >= 1000){
      return Math.floor(points/1000) + "K"
    }
    else{
      return points
    }
 }

  avatarTitle(){
    let {name,surname} = this.props
    if(name && surname){
      let name = this.props.name.charAt(0)
      let surname = this.props.surname.charAt(0)
      return name+surname
    }
  }


  avatar(){
    if(this.props.profilePic == "" || this.props.profilePic == null){
      return(
        <Avatar
        rounded
        size="large"
        title={this.avatarTitle()}
        onPress={() => this.props.navigation.navigate('profile')} 
        activeOpacity={0.7}
        />            
      )
    }
    else{
      return(
      <Avatar
      source={{
        uri: url[this.props.u].image + this.props.profilePic,
      }}
      rounded
      size="large"
      onPress={() => this.props.navigation.navigate('profile')} 
      activeOpacity={0.7}
      />   
      )         
    }
  }


  message(){
    if(this.props.gender == 'Femmina'){
      return 'BENVENUTA'
    }
    else{
      return 'BENVENUTO'
    }
  }

    render() {
      console.log(this.props.u)
      return (
        <SafeAreaView style={styles.container}>
            {/* <Spinner
            visible={this.state.isLoading}
            textContent={'Caricando...'}
            color={"#f3f3f3"}
            textStyle={styles.spinnerTextStyle}
            /> */}
            <Overlay
            isVisible={this.state.isVisible}
            windowBackgroundColor="rgba(255, 255, 255, .5)"
            overlayBackgroundColor={Colors.primary}
            width='85%'
            height="auto"
            borderRadius={0}
            onBackdropPress={() => this.setState({isVisible:false})}
            containerStyle={{padding:0}}
            >
          <Site />
          </Overlay>
          <View style={{flex:1}}>
          <Image
          style={{width: 230, height: '100%'}}
          source={require('./../assets/images/logo.png')}
        />

            </View>
          <View style={{flex:1,flexDirection:'row', marginBottom:4}}>
          <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
              {this.avatar()}
          </View>
          <View style={{flex:2, justifyContent:'center', alignItems:'center'}}>
            <Text style={{fontSize:22, fontFamily:Fonts.MontserratSemiBold, }}>{this.message()}</Text>
            <Text style={{fontSize:20, fontFamily:Fonts.MontserratSemiBold, lineHeight:22 }}>{this.props.name}</Text>
          </View>
          <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
            <Text style={{fontSize:20, fontFamily:Fonts.MontserratSemiBold,color:Colors.primary }}>{this.k(this.props.points)}</Text>
            <Text style={{fontSize:16, fontFamily:Fonts.MontserratSemiBold, lineHeight:16 }}>Points</Text>
          </View>
          </View>
          <View style={{flex:5, width:'100%'}}>
            <Image source={gyms[this.props.u].background} style={{width:'100%', height:'100%'}} />
          </View>
          <View style={{flex:1, justifyContent:'center'}}>
          <TouchableOpacity 
            onPress={() => this.setState({isVisible:true})}
            // onPress={() => this.props.navigation.navigate('TrainingDetails', {training_id:1})}
            style={{borderColor: '#F08019',width:230,display: 'flex',height: '70%',justifyContent: 'center',alignItems: 'center',borderWidth:2,borderColor:Colors.primary}}>
              <Text style={{ fontSize: 20, fontFamily: Fonts.MontserratSemiBold, color:Colors.primary}}>IL TUO CLUB</Text>
          </TouchableOpacity>
          </View>
        </SafeAreaView>
      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'center',
        alignItems:'center'
    },
    spinnerTextStyle:{
      color:"#f3f3f3",
      fontSize:15     
  }
  });

  const mapStateToProps = state => {
    return {
      token:state.authReducer.token,
      name: state.userReducer.username,
      surname: state.userReducer.surname,
      phone: state.userReducer.phone,
      email: state.userReducer.email,
      gender: state.userReducer.gender,
      dateofbirth: state.userReducer.dateofbirth,
      profilePic: state.userReducer.profilePic,
      points: state.userReducer.points,
      u: state.userReducer.palestre
  }
}

const mapDispachToProps = dispatch => {
  return bindActionCreators({
      setProfilePhoto,
      setUserName,
      setUserNickname,
      setUserSurname,
      setUserPhone,
      setUserEmail,
      setUserGender,
      setUserDateOfBirth,
      setUserPoints
 },dispatch)
}

export default connect(mapStateToProps,mapDispachToProps)(HomeScreen)