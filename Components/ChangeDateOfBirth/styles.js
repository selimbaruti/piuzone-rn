import { StyleSheet} from 'react-native';
import Colors from './../../Constants/Colors'
import Fonts from './../../Common/Fonts'

export default StyleSheet.create({
    container:{
        flex: 1,
    },
    header:{
        flex:1,
        paddingTop:'4%',
    },
    body:{
        flex: 10
    },
    top:{
        flex:4,
    },
    bottom:{
        flex:1,
        justifyContent:'flex-end',
    },
    icontainer:{
        paddingLeft:'2%',
        paddingRight:'2%',
    },
    info:{
        fontSize:16,
        marginBottom:10,
        color:'#8b8d8b',
        fontFamily: Fonts.MontserratMedium
    },
    input: {
        width:'100%',
        display:"flex",
        justifyContent:'center',
        height: 50,
        backgroundColor: Colors.background,
        paddingHorizontal:10,
        color:"#000",
        borderBottomWidth:2,
        borderBottomColor: Colors.primary ,
        // marginBottom:20,
      },
      bcontainer:{
        alignItems:'center', 
    },
    date:{
        fontFamily: Fonts.MontserratMedium,
        fontSize:17,
    },
    button:{
        width:'100%', 
        backgroundColor: Colors.primary, 
        display: 'flex', 
        height: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text:{
        fontSize: 20,
        color: '#FFFFFF',
        justifyContent:'center'
    },
    spinnerTextStyle:{
        color:"#f3f3f3",
        fontSize:15     
    }
  });