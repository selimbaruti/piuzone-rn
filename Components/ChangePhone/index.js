import React, {Component} from 'react';
import { View,TextInput,TouchableOpacity,Text} from 'react-native';
import styles from './styles'
import Spinner from 'react-native-loading-spinner-overlay';
import instance from './../../api/instance'
import { connect } from "react-redux";
import { setUserPhone } from "./../../store/actions/auth";
import { bindActionCreators } from "redux";
import Header from './../Header/index'
import { showMessage, hideMessage } from "react-native-flash-message";

class ChangePhone extends Component {
    constructor(props){
        super(props);
        this.state = {
            isLoading:null,
            phone: this.props.phone
        }
    }

    send(){
        this.setState({isLoading:true})
        instance.post('user/update', {
            cellulare: this.state.phone
        })
        .then(response => {
            this.props.setUserPhone(this.state.phone)
            this.setState({isLoading:false})
            this.props.navigation.goBack()
        })
        .catch(err => {
            this.setState({isLoading:false})
            showMessage({
                message:err.response ? err.response.data.msg : 'La modifica non è andata a buon fine',
                type: "danger",
              });
        })
        // setTimeout(() =>{
        //     this.setState({isLoading:false})
        // },3000);
    }

    render() {
      return (
        <View style={styles.container}>
            <Spinner
            visible={this.state.isLoading}
            textContent={'Salvando...'}
            color={"#f3f3f3"}
            textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.header}>
            <Header title="Cellulare" back one navigation={this.props.navigation} />
            </View>
            <View style={styles.body}>
            <View style={styles.top}>
                <View style={styles.icontainer}>
                    <Text style={styles.info}>Modifica il numero di telefono</Text>
                    <TextInput style={styles.input}
                    value={this.state.phone}
                    keyboardType={'number-pad'}
                    returnKeyType={'done'}
                    onChangeText={TextInputValue =>
                        this.setState({phone : TextInputValue }) }
                    />
                </View>
            </View>
            <View style={styles.bottom}>
                <View style={[styles.bcontainer]}>
                    <TouchableOpacity 
                    activeOpacity={0.8}
                    style={styles.button}
                    onPress={() => {
                        if(this.state.phone != ''){
                            this.send()
                            }
                        else{
                            showMessage({
                                message:'Questo campo non può essere vuoto',
                                type: "warning",
                            });                
                        }           
                        }}>
                        <Text style={styles.text}>Salva</Text>
                    </TouchableOpacity>
                </View>          
            </View>
            </View>
        </View>
      )
    }
  }
  
const mapStateToProps = state => {
    return{
        phone: state.userReducer.phone,
    }
}

const mapDispachToProps = dispatch => {
    return bindActionCreators({
        setUserPhone
   },dispatch)
}

export default connect(mapStateToProps,mapDispachToProps)(ChangePhone)