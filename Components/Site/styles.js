import { StyleSheet} from 'react-native';
import Fonts from './../../Common/Fonts'
import Colors from './../../Constants/Colors'

export default StyleSheet.create({
    container:{
        flex: 1,
    },
    box:{
        width:'100%', 
        height:70,
        justifyContent:'center',
    },
    text:{
        marginLeft:5,
        fontSize:19,
        fontFamily:Fonts.MontserratMedium,
        textAlign:'center',
        color: Colors.white
    }
  });