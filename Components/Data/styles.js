import { StyleSheet} from 'react-native';
import Fonts from './../../Common/Fonts'
import Colors from './../../Constants/Colors'

export default StyleSheet.create({
    container:{
        flex: 1,
    },
    top:{
        flex:4,
    },
    bottom:{
        flex:1,
        justifyContent:'flex-end',
    },
    bcontainer:{
        alignItems:'center', 
    },
    button:{
        width:'100%', 
        backgroundColor: Colors.primary, 
        display: 'flex', 
        height: 60,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text:{
        fontSize: 20,
        color: '#FFFFFF',
        fontFamily:Fonts.MontserratMedium
    },
    title:{
        fontFamily: Fonts.MontserratMedium
    },
    righttitle:{
        textAlign: "right",
        width: 290,
        fontFamily: Fonts.MontserratMedium
    }
  });