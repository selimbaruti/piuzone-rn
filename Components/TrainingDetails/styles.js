import { StyleSheet, Platform, Dimensions } from 'react-native'

export default StyleSheet.create({
    container: {
        flex:1,
    },
    trainingInfoB:{
        flex:3,
        flexDirection:'row',
        marginTop: '3%'
    },
    traininginfoS: {
        flex:4,
        flexDirection: 'row'
    },
    stats:{
        flex:7,
    },
    loader:{
        marginTop:'2%'
    }
})
