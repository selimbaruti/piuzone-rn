import React, {Component} from 'react';
import { View,ImageBackground,ActivityIndicator } from 'react-native';
import styles from './styles'
import Images from './../../Common/Images'
import TrainingInfo from './../../Components/TrainingInfo/index'
import Stats from './../../Components/Stats/index'
import instance from './../../api/instance'
import Colors from '../../Constants/Colors';
import { ScrollView } from 'react-native-gesture-handler';


export default class Telemetry extends Component {

   
   constructor(props) {
    super(props);
    this.state = {
        loading: true,
    };
   }
   
   componentDidMount(){
    this.getTotal()
 }

 getTotal(){
   instance.get('telemetria/totalsByRegistryId')
     .then(response => {
         this.setState({data:response.data, loading:false})
     }).catch(e => {
       console.log(e.response);
     })
   }

   k(calories){
      if(calories >= 1000000000){
        return Math.floor(calories/1000000000) + "B"
      }
      else if(calories >= 1000000){
        return Math.floor(calories/1000000) + "M"
      }
      else if(calories >= 1000){
        return Math.floor(calories/1000) + "K"
      }
      else{
        return calories
      }
   }

    render() {
      if(this.state.loading){
        return( 
          <ImageBackground source={require('./../../assets/images/fondo_sfumato.png')} style={styles.container}>
            <View style={styles.loader}> 
            <ActivityIndicator size="large" color={Colors.primary}/>
            </View>
          </ImageBackground>
      )}
      
        return (
            <ImageBackground source={require('./../../assets/images/fondo_sfumato.png')} style={styles.container}>
              <ScrollView>
            <View style={styles.trainingInfoB}>
                <TrainingInfo info={this.k(this.state.data.calories)} percentage={false} description="KCAL" time={"(" + this.state.data.calMins + " kcal/min" + ")"} icon={Images.icons.kcal} big={true}/>
                <TrainingInfo info={this.k(this.state.data.points)} percentage={false} description="POINTS" time={"(" + this.state.data.points_x_minute + " points/min" + ")"} icon={Images.icons.points} big={true}/>
            </View>
            <View style={styles.traininginfoS}>
                <TrainingInfo info={this.state.data.duration} percentage={false} description="DURATA" time="" icon={Images.icons.time} big={false}/>
                <TrainingInfo info={this.state.data.avgHr} percentage={false} description="FC MEDIA" time="" icon={Images.icons.beat} big={false} />
                <TrainingInfo info={this.state.data.tgzone} percentage={true} description="TARGET ZONE" time={"(" + this.state.data.tgZoneDuration + ")"} icon={Images.icons.target_zone} big={false}/>
            </View>
              <View style={styles.stats}>
              <Stats title="Aumento potenza" percentage={this.state.data.zoneDuration['z4'].prc} time={this.state.data.zoneDuration['z4'].duration} color="#d1171a" />
                <Stats title="Incremento metabolismo" percentage={this.state.data.zoneDuration['z3'].prc} time={this.state.data.zoneDuration['z3'].duration} color="#f08019" />
                <Stats title="Controllo del peso" percentage={this.state.data.zoneDuration['z2'].prc} time={this.state.data.zoneDuration['z2'].duration} color="#47a833" />
                <Stats title="Attivazione" percentage={this.state.data.zoneDuration['z1'].prc } time={this.state.data.zoneDuration['z1'].duration} color="#0099ce" />
               </View>
               </ScrollView>
            </ImageBackground>
        )
    }
}