import React from 'react'
import { Text,View,TouchableOpacity,Dimensions,Animated,Platform,StyleSheet,TextInput,SafeAreaView,KeyboardAvoidingView,TouchableWithoutFeedback,Keyboard} from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import {tryAuth} from '../../store/actions/auth';
import Colors from './../../Constants/Colors'
import RNPickerSelect from 'react-native-picker-select';
import { showMessage } from 'react-native-flash-message';
import { gyms } from './../../Constants/Gyms'

const windowWidth = Dimensions.get('window').width;
// palestre = [
//   {label:'Area Donna', value:'ad'}, 
//   {label:'Curno', value:'c'}, 
//   {label:'Cenate Sotto',value:'cs'}
// ]

class SignInScreen extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            email:'',
            password: '',
            palestre: 0
        }
        this.imageWidth = new Animated.Value(windowWidth*0.9);
    }

    static navigationOptions = {
      header: null,
    };


    keyboardWillShow = (event) => {
      Animated.timing(this.imageWidth, {
        duration: event.duration,
        toValue: windowWidth*0.6,
      }).start();
    };
  
    keyboardWillHide = (event) => {
      Animated.timing(this.imageWidth, {
        duration: event.duration,
        toValue: windowWidth*0.9,
      }).start();
    };
  
      componentWillMount(){
        this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
        this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
      }
  

    componentDidUpdate(prevProps){
        if(prevProps.token != this.props.token){
        this.props.navigation.navigate('AuthLoading')
        }
    }

    signin = () => {
      if(this.state.palestre == 0){
        showMessage({
          message: 'È necessario selezionare prima la palestra alla quale vuole accedere',
          type: "danger",
        })
      }
      else{
      const authData = {
          email: this.state.email,
          password: this.state.password
        };      
       this.props.tryAuth(authData,this.state.palestre)
      }
    };

    render(){
        // if(this.props.isLoading){
        //     return(
        //         <View style={styles.container}>
        //             <ActivityIndicator size="large" color="#F08019"/>
        //         </View>
        //     )
        // }
        console.log(this.state.palestre)
        return(
            <SafeAreaView style={styles.container}>
              <Spinner
              visible={this.props.isLoading}
              textContent={'Caricando...'}
              color={"#f3f3f3"}
              textStyle={styles.spinnerTextStyle}
              />
              <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : null} style={styles.container}>
              <TouchableWithoutFeedback style={styles.container}>
                  <View style={styles.container}>
                    <View  style={styles.logoContainer}>
                      <Animated.Image style={[styles.logo, { width: this.imageWidth }]} source={require('../../assets/images/logo.png')}/>
                    </View>
                    <View style={styles.infoContainer}>
                    <View style={{display:'flex', alignSelf:"center", width:'90%', marginBottom:20}}>
                        <RNPickerSelect
                          style={pickerSelectStyles}
                          onValueChange={(value) => {
                            this.setState({palestre:value})
                          }}    
                          items={gyms}
                          placeholder={{label:"Scegli la tua palestra ...", value:0}}
                          useNativeAndroidPickerStyle={false}
                          value={this.state.palestre}
                        />
                        </View>
                        <TextInput style={styles.input}
                          placeholder="Email"
                          placeholderTextColor="#000"
                          keyboardType="email-address"
                          autoCorrect={false}
                          returnKeyType="next"
                          autoCapitalize="none"
                          onSubmitEditing={()=>this.passwordInput.focus()}
                          onChangeText={ TextInputValue =>
                            this.setState({email : TextInputValue }) }
                        />
                        <TextInput style={styles.input}
                          placeholder="Password"
                          placeholderTextColor="#000"
                          keyboardType="default"
                          autoCapitalize="none"
                          autoCorrect={false}
                          returnKeyType="go"
                          secureTextEntry
                          ref={(input)=>this.passwordInput = input}
                          onChangeText={ TextInputValue =>
                            this.setState({password : TextInputValue }) }
                        />
                        <TouchableOpacity style={styles.buttonContainer}  
                        onPress={this.signin}
                        >
                          <Text style={styles.buttonText}>Entra</Text>
                        </TouchableOpacity>
                        <View style={styles.signupTextCont}>
                          <Text style={styles.signupText}>Non sei ancora registrato?</Text>
                          <TouchableOpacity
                          onPress={() => this.props.navigation.navigate('Register')}
                          ><Text style={styles.signupButton}> Registrati</Text></TouchableOpacity>
                        </View>
                        <View style={styles.signupTextCont}>
                         <TouchableOpacity onPress={() => this.props.navigation.navigate('LostPassword')}><Text style={[styles.signupButton,styles.signupText,{paddingTop:0}]}> Hai dimenticato la password?</Text></TouchableOpacity>
                        </View>
                    </View>
                  </View>
                </TouchableWithoutFeedback>
              </KeyboardAvoidingView>
            </SafeAreaView>
          )
    }
}

const styles = StyleSheet.create({
    container:{
      flex:1,
      // backgroundColor: Colors.primary,
      backgroundColor: '#ffffff',
    },
    spinnerTextStyle:{
      color:"#f3f3f3",
      fontSize:15
    },  
    logoContainer:{
      alignItems:'center',
      justifyContent:'center',
      flex:1,
    },
    logo:{
      flex:1,
      resizeMode:'contain',
      //width:windowWidth*0.7,
      
      //paddingBottom:60
    },
    infoContainer:{
      display:'flex',
      alignItems:'center',
      padding:0
    },
    input: {
      width:'90%',
      height: 50,
      backgroundColor: '#f2f3f2',
      color:"#000",
      paddingHorizontal:10,
      marginBottom:20
    },
    buttonContainer:{
        backgroundColor:Colors.primary,
        paddingVertical:15,
        width:'90%'
    },
    buttonText:{
      textAlign:'center',
      fontSize:18,
      color:"#fff",
      fontWeight:"bold"
    },
    signupTextCont : {
      flexGrow: 1,
      alignItems:'flex-end',
      justifyContent :'center',
      paddingVertical:5,
      
      flexDirection:'row'
    },
    signupText: {
        color:'#b1b4b1',
        fontSize:16
    },
    signupButton: {
        fontSize:16,
        color:'#b1b4b1',
        fontWeight:'bold'
    }
 });
  
 const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize:15, 
    color: '#000',
    backgroundColor:'#f2f3f2',
    height:50,
    paddingHorizontal:10,
  },
  inputAndroid: {
    fontSize:15, 
    color: '#000',
    backgroundColor:'#f2f3f2',
    height:50,
    paddingHorizontal:10,
  },
  placeholder:{
    color:'#000'
  }
  });

 const mapStateToProps = state => {
  return{
      token: state.authReducer.token,
      isLoading: state.uiReducer.isLoading
  }
}

const mapDispachToProps = dispatch => {
  return bindActionCreators({
      tryAuth
 },dispatch)
}

export default connect(mapStateToProps,mapDispachToProps)(SignInScreen)