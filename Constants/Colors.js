export default Colors = {
    primary: '#bc1e2c',
    secondary: '#d7d8d7',
    white:'#ffffff',
    background:'#f2f3f2'
}
